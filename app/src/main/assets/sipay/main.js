function myFunction(payload) {
    window.location = 'about:blank'
    var payload_url = Android.authenticate(payload.request_id);
    console.log(payload_url);
    window.location = payload_url;
}

Fastpay.customize({
  colors: {
    title: "#007bff",
    description: "#007bff",
    header_line: "#007bff",
    inputs_lines: "#007bff",
    icon: "#007bff",
    info: "#007bff",
    button: "blue",
    cvv_info: "#c0c0c0",
    header_color: "#007bff",
  },
});

document.addEventListener("DOMContentLoaded", function () {
  document.querySelector(".fastpay-btn").click();
});
