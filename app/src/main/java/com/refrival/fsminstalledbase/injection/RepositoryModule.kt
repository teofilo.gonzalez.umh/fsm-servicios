package com.refrival.fsminstalledbase.injection

import com.refrival.fsminstalledbase.repository.db.*
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideSAPRepository(
        ordenDao: OrdenDao,
        operacionDao: OperacionDao,
        jerarquiaDao: JerarquiaDao,
        detalleOperacionDao: DetalleOperacionDao,
        jerProAutoDao: JerProAutoDao,
        jerProNivelDao: JerProNivelDao,
        baseInstaladaDao: BaseInstaladaDao,
        motivoCierreDao: MotivoCierreDao,
        ayudaBusquedaDao: AyudaBusquedaDao,
        stockDao: StockDao,
        materialDao: MaterialDao,
        materialesProyectoDao: MaterialesProyectoDao,
        ): SAPRepository {
        return SAPRepository(
            ordenDao,
            operacionDao,
            jerarquiaDao,
            detalleOperacionDao,
            jerProAutoDao,
            jerProNivelDao,
            baseInstaladaDao,
            motivoCierreDao,
            ayudaBusquedaDao,
            stockDao,
            materialDao,
            materialesProyectoDao
        )
    }
}