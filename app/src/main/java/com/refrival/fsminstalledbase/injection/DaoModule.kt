package com.refrival.fsminstalledbase.injection

import com.refrival.fsminstalledbase.repository.db.*
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DaoModule {
    @Provides
    @Reusable
    fun provideOrdenDao(refrivalDatabase: RefrivalDatabase): OrdenDao {
        return refrivalDatabase.ordenDao()
    }

    @Provides
    @Reusable
    fun provideOperacionDao(refrivalDatabase: RefrivalDatabase): OperacionDao {
        return refrivalDatabase.operacionDao()
    }

    @Provides
    @Reusable
    fun provideJerarquiaDao(refrivalDatabase: RefrivalDatabase): JerarquiaDao {
        return refrivalDatabase.jerarquiaDao()
    }

    @Provides
    @Reusable
    fun provideDetalleOperacionDao(refrivalDatabase: RefrivalDatabase): DetalleOperacionDao {
        return refrivalDatabase.detalleOperacionDao()
    }

    @Provides
    @Reusable
    fun provideJerProAutoDao(refrivalDatabase: RefrivalDatabase): JerProAutoDao {
        return refrivalDatabase.jerProAutoDao()
    }

    @Provides
    @Reusable
    fun provideJerProNivelDao(refrivalDatabase: RefrivalDatabase): JerProNivelDao {
        return refrivalDatabase.jerProNivelDao()
    }

    @Provides
    @Reusable
    fun provideBaseInstaladaDao(refrivalDatabase: RefrivalDatabase): BaseInstaladaDao {
        return refrivalDatabase.baseInstalledDao()
    }

    @Provides
    @Reusable
    fun provideMotivoCierreDao(refrivalDatabase: RefrivalDatabase): MotivoCierreDao {
        return refrivalDatabase.motivoCierreDao()
    }

    @Provides
    @Reusable
    fun provideAyudaBusquedaDao(refrivalDatabase: RefrivalDatabase): AyudaBusquedaDao {
        return refrivalDatabase.ayudaBusquedaDao()
    }

    @Provides
    @Reusable
    fun provideStockDao(refrivalDatabase: RefrivalDatabase): StockDao {
        return refrivalDatabase.stockDao()
    }

    @Provides
    @Reusable
    fun provideMaterialDao(refrivalDatabase: RefrivalDatabase): MaterialDao {
        return refrivalDatabase.materialDao()
    }

    @Provides
    @Reusable
    fun provideMaterialesProyectoDao(refrivalDatabase: RefrivalDatabase): MaterialesProyectoDao {
        return refrivalDatabase.materialesProyectoDao()
    }
}