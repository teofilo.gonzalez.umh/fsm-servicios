package com.refrival.fsminstalledbase.injection

import android.content.Context
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun providesRefrivalDatabase(@ApplicationContext context: Context): RefrivalDatabase {
        return RefrivalDatabase.getInstance(context)
    }
}