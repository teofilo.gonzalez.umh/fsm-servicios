package com.refrival.fsminstalledbase.utils

import org.apache.commons.codec.binary.Hex
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

class Utils {
    companion object {
        fun createSignature(data: String, key: String): String {
            val sha256Hmac = Mac.getInstance("HmacSHA256")
            val secretKey = SecretKeySpec(key.toByteArray(), "HmacSHA256")
            sha256Hmac.init(secretKey)

            return String(Hex.encodeHex(sha256Hmac.doFinal(data.toByteArray())))
        }
    }
}