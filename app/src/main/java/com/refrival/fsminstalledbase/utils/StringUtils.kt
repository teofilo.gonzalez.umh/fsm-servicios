package com.refrival.fsminstalledbase.utils

import org.apache.commons.codec.binary.Base64

class StringUtils {
    companion object {
        fun toBase64(input: String?) : String? {
            if (input == null) {
                return null
            }

            val base64 = Base64()
            return String(base64.encode(input.toByteArray()))
        }
    }
}