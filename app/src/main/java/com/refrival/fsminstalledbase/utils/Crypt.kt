package com.refrival.fsminstalledbase.utils

import android.util.Base64
import java.io.UnsupportedEncodingException
import java.lang.Exception
import java.lang.RuntimeException
import java.security.Key
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

object Crypt {

    private const val IV = "IV_VALUE_16_BYTE"
    private const val PASSWORD = "ewLls2jjMmm"
    private const val SALT = "0123456789abcdef"

    fun encryptAndEncode(raw: String): String? {
        return try {
            val c: Cipher = getCipher(Cipher.ENCRYPT_MODE)
            val encryptedVal: ByteArray = c.doFinal(getBytes(raw))
            Base64.encodeToString(encryptedVal, Base64.DEFAULT)
        } catch (t: Throwable) {
            throw RuntimeException(t)
        }
    }

    @Throws(Exception::class)
    fun decodeAndDecrypt(encrypted: String): String {
        val decodedValue: ByteArray = Base64.decode(getBytes(encrypted), Base64.DEFAULT)
        val c: Cipher = getCipher(Cipher.DECRYPT_MODE)
        val decValue: ByteArray = c.doFinal(decodedValue)
        return String(decValue)
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getString(bytes: ByteArray): String {
        return String(bytes, charset("UTF-8"))
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getBytes(str: String): ByteArray {
        return str.toByteArray(charset("UTF-8"))
    }

    @Throws(Exception::class)
    private fun getCipher(mode: Int): Cipher {
        val c: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        val iv = getBytes(IV)
        c.init(mode, generateKey(), IvParameterSpec(iv))
        return c
    }

    @Throws(Exception::class)
    private fun generateKey(): Key {
        val factory: SecretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        val password = PASSWORD.toCharArray()
        val salt = getBytes(SALT)
        val spec: KeySpec = PBEKeySpec(password, salt, 65536, 128)
        val tmp: SecretKey = factory.generateSecret(spec)
        val encoded: ByteArray = tmp.encoded
        return SecretKeySpec(encoded, "AES")
    }
}