package com.refrival.fsminstalledbase.utils

// Constants
const val TAG = "fsminstalledbase"
// The authority for the sync adapter's content provider
const val AUTHORITY = "com.refrival.fsminstalledbase.provider"
// An account type, in the form of a domain name
const val ACCOUNT_TYPE = "com.refrival.fsminstalledbase"
// The account name
const val ACCOUNT = "FSM Servicios"
// Incoming Intent key for extended data
const val KEY_SYNC_REQUEST = "com.refrival.fsminstalledbase.datasync.KEY_SYNC_REQUEST"

const val BASE_URL = "https://auth.coresuite.com"
const val CLIENT_SECRET = "b4a4cdb5-beef-4eea-abc7-0231737fec1a"
const val CLIENT_ID = "clienttechedge123123"
const val GRANT_TYPE = "password"
const val RESPONSE_TYPE = "token"

const val SAP_BASE_URL = "https://s4df.refrival.com/sap/opu/odata/sap/ZPROJ_FSM_S4_SRV/"
const val SAP_USER = "EXTJOAMON01"
const val SAP_USER_TECNICO_1 = "TGONZALES"
const val SAP_USER_TECNICO_2 = "JMONSERRAT"
const val SAP_PASSWORD = "Techedge02#"

const val UNIQUE_SYNC_WORK_NAME = "syncWork"
const val UNIQUE_PERIODIC_WORK_NAME = "periodicWork"
const val ETAPA_LINEA_AYUDA = "etapaLinea"

// TODO no poner a pelo estos dos
const val TECNICO_ID = "FJV"
const val DEVICE_ID = "eGRMrsOMRjK8WYctuzEqYs:APA91bH4X19dRi2zS_XEdtPDfD5nnGl5YK0fxTADgzUT7nxcgTsBbpcxkR1Hppk3O7ZIqucNWmBhHSmS2CXlEGDqw5WcYt2FiswWNtdpKkG8I_zRPHL2JL1uXDCKedQqHKg-jtaP2CY8"

const val ACCION_BI_DESMONTAR = "D"
const val ACCION_BI_MONTAR = "M"
const val ACCION_BI_SELECCIONAR = "S"
const val ACCION_BI_MODIFICAR = "A"

const val CLAVE_CLASIFICACION_ACTIVO = "ACTIVO"
