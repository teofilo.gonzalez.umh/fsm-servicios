package com.refrival.fsminstalledbase.utils

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.refrival.fsminstalledbase.ui.closingrequirements.ClosingRequirementsViewModel
import com.refrival.fsminstalledbase.ui.faultcauses.FaultCausesViewModel
import com.refrival.fsminstalledbase.ui.header.HeaderViewModel
import com.refrival.fsminstalledbase.ui.operations.OperationsViewModel

class CustomViewModelFactory(
    private val application: Application,
    private val owner: LifecycleOwner
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return with(modelClass) {
            when {
                isAssignableFrom(OperationsViewModel::class.java) -> OperationsViewModel(application, owner)
                isAssignableFrom(FaultCausesViewModel::class.java) -> FaultCausesViewModel(application, owner)
                isAssignableFrom(ClosingRequirementsViewModel::class.java) -> ClosingRequirementsViewModel(application, owner)
                isAssignableFrom(HeaderViewModel::class.java) -> HeaderViewModel(application, owner)
                else -> throw IllegalArgumentException("Unknown ViewModel")
            }
        } as T
    }
}