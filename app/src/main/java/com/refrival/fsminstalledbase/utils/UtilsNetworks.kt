package com.refrival.fsminstalledbase.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.telephony.ServiceState
import android.telephony.TelephonyManager
import java.lang.Exception

object UtilsNetworks {

    enum class NetworkType {
        NONE, WIFI, MOBILE
    }

    private fun getNetwork(connectivityManager: ConnectivityManager, type: Int): Network? {
        try {
            val networkInfoToFind = connectivityManager.getNetworkInfo(type)
            for (net in connectivityManager.allNetworks) {
                val networkInfo = connectivityManager.getNetworkInfo(net)
                if (networkInfoToFind != null && networkInfo != null) if (networkInfoToFind.type == networkInfo.type) return net
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun getWifiNetwork(connectivityManager: ConnectivityManager): Network? {
        return getNetwork(connectivityManager, ConnectivityManager.TYPE_WIFI)
    }

    fun getMobileNetwork(connectivityManager: ConnectivityManager): Network? {
        return getNetwork(connectivityManager, ConnectivityManager.TYPE_MOBILE)
    }

    /**
     * Method to check if Wifi interface is enabled / switched on
     *
     * @param wifiManager wifi
     * @return true if enabled otherwise false
     */
    fun isWifiEnabled(wifiManager: WifiManager): Boolean {
        return wifiManager.isWifiEnabled
    }

    /**
     * Method to check if Mobile interface is enabled / switched on
     *
     * @param serviceState service
     * @return true if enabled otherwise false
     */
    fun isMobileEnabled(serviceState: ServiceState): Boolean {
        return serviceState.state == ServiceState.STATE_IN_SERVICE
    }

    /**
     * Method to know what interface (Wifi or Mobile) is connected to data
     *
     * @param connectivityManager connectivity
     * @return NetworkType WIFI when data is via Wifi, MOBILE when data is via mobile, otherwise NONE
     */
    fun getConnectedNetwork(connectivityManager: ConnectivityManager): NetworkType {
        val wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (wifi != null && wifi.isConnected) {
            return NetworkType.WIFI
        }
        return if (mobile != null && mobile.isConnected) {
            NetworkType.MOBILE
        } else NetworkType.NONE
    }

    /**
     * Get the network info
     *
     * @param context this
     * @return network info
     */
    private fun getNetworkInfo(context: Context): NetworkInfo? {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo
    }

    /**
     * Check if there is any connectivity
     *
     * @param context this
     * @return true/false
     */
    fun isConnected(context: Context): Boolean {
        val info = getNetworkInfo(context)
        return info != null && info.isConnected
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context this
     * @return true/false
     */
    fun isConnectedWifi(context: Context): Boolean {
        val info = getNetworkInfo(context)
        return info != null && info.isConnected && info.type == ConnectivityManager.TYPE_WIFI
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context this
     * @return true/false
     */
    fun isConnectedMobile(context: Context): Boolean {
        val info = getNetworkInfo(context)
        return info != null && info.isConnected && info.type == ConnectivityManager.TYPE_MOBILE
    }

    /**
     * Check if there is fast connectivity
     *
     * @param context this
     * @return true/false
     */
    fun isConnectedFast(context: Context): Boolean {
        val info = getNetworkInfo(context)
        return info != null && info.isConnected && isConnectionFast(info.type, info.subtype)
    }

    /**
     * Check if the connection is fast
     *
     * @param type    this
     * @param subType type
     * @return
     */
    private fun isConnectionFast(type: Int, subType: Int): Boolean {
        return when (type) {
            ConnectivityManager.TYPE_WIFI -> {
                true
            }
            ConnectivityManager.TYPE_MOBILE -> {
                when (subType) {
                    TelephonyManager.NETWORK_TYPE_1xRTT -> false // ~ 50-100 kbps
                    TelephonyManager.NETWORK_TYPE_CDMA -> false // ~ 14-64 kbps
                    TelephonyManager.NETWORK_TYPE_EDGE -> false // ~ 50-100 kbps
                    TelephonyManager.NETWORK_TYPE_EVDO_0 -> true // ~ 400-1000 kbps
                    TelephonyManager.NETWORK_TYPE_EVDO_A -> true // ~ 600-1400 kbps
                    TelephonyManager.NETWORK_TYPE_GPRS -> false // ~ 100 kbps
                    TelephonyManager.NETWORK_TYPE_HSDPA -> true // ~ 2-14 Mbps
                    TelephonyManager.NETWORK_TYPE_HSPA -> true // ~ 700-1700 kbps
                    TelephonyManager.NETWORK_TYPE_HSUPA -> true // ~ 1-23 Mbps
                    TelephonyManager.NETWORK_TYPE_UMTS -> true // ~ 400-7000 kbps
                    TelephonyManager.NETWORK_TYPE_EHRPD -> true // ~ 1-2 Mbps
                    TelephonyManager.NETWORK_TYPE_EVDO_B -> true // ~ 5 Mbps
                    TelephonyManager.NETWORK_TYPE_HSPAP -> true // ~ 10-20 Mbps
                    TelephonyManager.NETWORK_TYPE_IDEN -> false // ~25 kbps
                    TelephonyManager.NETWORK_TYPE_LTE -> true // ~ 10+ Mbps
                    TelephonyManager.NETWORK_TYPE_UNKNOWN -> false
                    else -> false
                }
            }
            else -> {
                false
            }
        }
    }
}