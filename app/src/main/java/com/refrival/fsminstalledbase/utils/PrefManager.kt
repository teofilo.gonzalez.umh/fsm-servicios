package com.refrival.fsminstalledbase.utils

import android.content.Context
import android.content.SharedPreferences


class PrefManager(context: Context?) {

    companion object {
        const val REFRIVAL = "REFRIVAL"
        const val FIREBASE_TOKEN = "FIREBASE_TOKEN"
        const val COOKIE = "COOKIE"
        const val ORDEN_ID = "ORDEN_ID"
        const val OPERACION_ID = "OPERACION_ID"
        const val IS_FIRST_SYNC = "IS_FIRST_SYNC"
    }

    // Shared Preferences
    private val prefREFRIVAL: SharedPreferences = context!!.getSharedPreferences(REFRIVAL, Context.MODE_PRIVATE)

    // Editor for Shared preferences
    private val editorREFRIVAL: SharedPreferences.Editor = prefREFRIVAL.edit()

    /**
     * Get Firebase Token
     *
     * @return token
     */
    val getFirebaseToken: String?
        get() = prefREFRIVAL.getString(FIREBASE_TOKEN, "")

    /**
     * Save Firebase Token
     */
    fun saveFirebaseToken(token: String) {
        editorREFRIVAL.putString(FIREBASE_TOKEN, token)
        editorREFRIVAL.commit()
    }

    /**
     * Get Firebase Token
     *
     * @return token
     */
    val getCookie: String?
        get() = prefREFRIVAL.getString(COOKIE, "")

    /**
     * Save Firebase Token
     */
    fun saveCookie(cookie: String) {
        editorREFRIVAL.putString(COOKIE, cookie)
        editorREFRIVAL.commit()
    }

    val ordenId: String?
        get() = prefREFRIVAL.getString(ORDEN_ID, "")

    fun saveOrdenId(ordenId: String) {
        editorREFRIVAL.putString(ORDEN_ID, ordenId)
        editorREFRIVAL.commit()
    }

    val operacionId: String?
        get() = prefREFRIVAL.getString(OPERACION_ID, "")

    fun saveOperacionId(operacionId: String) {
        editorREFRIVAL.putString(OPERACION_ID, operacionId)
        editorREFRIVAL.commit()
    }

    val isFirstSync: Boolean
        get() = prefREFRIVAL.getBoolean(IS_FIRST_SYNC, true)

    fun saveIsFirstSync(isFirstSync: Boolean) {
        editorREFRIVAL.putBoolean(IS_FIRST_SYNC, isFirstSync)
        editorREFRIVAL.commit()
    }
}
