package com.refrival.fsminstalledbase

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import com.refrival.fsminstalledbase.repository.db.UserDao
import com.refrival.fsminstalledbase.repository.model.Operacion
import com.refrival.fsminstalledbase.repository.model.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject
constructor(
    refrivalDatabase: RefrivalDatabase
) : ViewModel() {

    private var refrivalRepository = refrivalDatabase.userDao()

    private val _tagId = MutableLiveData<String>()
    val tagId: LiveData<String>
        get() = _tagId

    private val _user = MutableLiveData<User>()
    val user: LiveData<User?>
        get() = _user

    fun setTagId(tagId: String) {
        _tagId.postValue(tagId)
    }

    fun cleanTagId() {
        _tagId.value = ""
    }

    fun getUser(owner: LifecycleOwner) {
        refrivalRepository.get().observe(owner) { user ->
            if (user != null) _user.value = user
        }
    }
}