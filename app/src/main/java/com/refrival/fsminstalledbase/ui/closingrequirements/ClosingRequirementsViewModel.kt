package com.refrival.fsminstalledbase.ui.closingrequirements

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel

class ClosingRequirementsViewModel(context: Application, private val owner: LifecycleOwner) : ViewModel() {

    var delegate: ClosingRequirementViewModelDelegate? = null

    fun init() {
        delegate?.initialize()
    }
}