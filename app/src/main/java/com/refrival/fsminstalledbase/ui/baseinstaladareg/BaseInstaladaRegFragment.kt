package com.refrival.fsminstalledbase.ui.baseinstaladareg

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.BIReg
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_installed_base.*

@AndroidEntryPoint
class BaseInstaladaRegFragment : Base.BaseFragment(),
    HeaderFragment.HeaderDelegate {

    private lateinit var ordenId: String
    private lateinit var operacionId: String

    private val viewModel: BaseInstaladaRegViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_installed_base
    }

    override fun init() {
        rvBaseInstalada.layoutManager = LinearLayoutManager(activity)

        showContentHeader()
        setHasOptionsMenu(true)

        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!
    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun initListeners() {
        (activity as MainActivity).onClickButtonMaterial()

        viewModel.getTreeBaseInstalada(ordenId, operacionId)
            .observe(viewLifecycleOwner) { baseInstalada ->
                rvBaseInstalada.adapter = BaseInstaladaRegAdapter(
                    baseInstalada,
                    BaseInstaladaRegAdapter.OnClickListener(this::onClickBaseInstalada),
                    BaseInstaladaRegAdapter.OnLongClickListener(this::onLongClickBaseInstalada)
                )
            }
    }

    private fun onLongClickBaseInstalada(bi: BIReg): Boolean {
        val actions: MutableList<String> = mutableListOf()
        actions.add(resources.getString(R.string.accion_montar))
        actions.add(resources.getString(R.string.accion_desmontar))
        actions.add(resources.getString(R.string.accion_reportar_averia))
        actions.add(resources.getString(R.string.accion_seleccionar))
        actions.add(resources.getString(R.string.accion_modificar))

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("${bi.baseInstalada?.textoComponente}")
            .setItems(actions.toTypedArray()) { dialog, which ->
                when (actions[which]) {
                    resources.getString(R.string.accion_modificar) -> findNavController().navigate(
                        BaseInstaladaRegFragmentDirections.actionBaseInstaladaRegFragmentToModificarDatosFragment(
                            idNodo = bi.baseInstalada?.idNodo!!,
                            textoComponente = bi.baseInstalada?.textoComponente!!,
                            numSerie = bi.baseInstalada?.numSerie!!,
                            tag = bi.baseInstalada?.numPiezaFabricante!!,
                            textoComponenteOriginal = bi.baseInstalada?.textoComponente!!,
                            numSerieOriginal = bi.baseInstalada?.numSerie!!,
                            tagOriginal = bi.baseInstalada?.numPiezaFabricante!!
                        )
                    )
                }
            }
            .show()

        return true
    }

    private fun onClickBaseInstalada(bi: BIReg) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.article_info))
            .setMessage(
                "${resources.getString(R.string.type)}: ${bi.baseInstalada?.tipoComponente}\n" +
                        "${resources.getString(R.string.name)}: ${bi.baseInstalada?.textoComponente}\n" +
                        "${resources.getString(R.string.serial_number)}: ${bi.baseInstalada?.numSerie}\n" +
                        "${resources.getString(R.string.tag)}: ${bi.baseInstalada?.numPiezaFabricante}\n" +
                        "${resources.getString(R.string.status)}: ${bi.baseInstalada?.claveClasificacion}\n" +
                        "${resources.getString(R.string.quantity)}: ${bi.baseInstalada?.cantidad}\n"
            )
            .show()
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.installed_base_menu, menu)

        menu.findItem(R.id.close_installed_base).isVisible = true
        menu.findItem(R.id.start_installed_base).isVisible = false
        menu.findItem(R.id.regularize).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.close_installed_base -> {
                onFinalizar()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onFinalizar() {
    }

    override fun goToServicePoint() {
        findNavController().navigate(BaseInstaladaRegFragmentDirections.actionBaseInstaladaRegFragmentToServicePointFragment())
    }
}