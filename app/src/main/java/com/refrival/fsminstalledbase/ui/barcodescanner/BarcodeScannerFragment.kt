package com.refrival.fsminstalledbase.ui.barcodescanner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.refrival.fsminstalledbase.R

class BarcodeScannerFragment : Fragment() {
    private lateinit var codeScanner: CodeScanner
    private lateinit var idPadre: String
    private var nivel: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_barcode_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val barcodeScannerFragmentArgs by navArgs<BarcodeScannerFragmentArgs>()
        idPadre = barcodeScannerFragmentArgs.idPadre
        nivel = barcodeScannerFragmentArgs.nivel

        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.decodeCallback = DecodeCallback {
            activity.runOnUiThread {
                val action = BarcodeScannerFragmentDirections.actionBarcodeScannerFragmentToAddMaterialFragment(barcode = it.text, idPadre = idPadre, nivel = nivel)
                findNavController().navigate(action)
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}