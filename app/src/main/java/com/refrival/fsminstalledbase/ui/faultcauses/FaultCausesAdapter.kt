package com.refrival.fsminstalledbase.ui.faultcauses

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import kotlinx.android.synthetic.main.item_fault_causes.view.*

class FaultCausesAdapter(val context: Context) : RecyclerView.Adapter<FaultCausesAdapter.FaultCausesHolder>() {

    private val causesList = mutableListOf<String>()

    class FaultCausesHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaultCausesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_fault_causes, parent, false)
        return FaultCausesHolder(view)
    }

    override fun onBindViewHolder(holder: FaultCausesHolder, position: Int) {
        causesList.let { item ->
            holder.view.txtCauses.text = item[position]
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setResume(resume: List<String>) {
        this.causesList.clear()
        this.causesList.addAll(resume)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return causesList.size
    }
}