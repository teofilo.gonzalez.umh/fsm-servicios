package com.refrival.fsminstalledbase.ui.modificardatos

import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ModificarDatosViewModel
@Inject constructor(
    private val baseInstaladaDao: BaseInstaladaDao,
) : ViewModel() {

    fun modificarElemento(
        idNodo: String,
        ordenId: String,
        operacionId: String,
        textoComponente: String,
        numSerie: String,
        tag: String,
    ) {
        baseInstaladaDao.modificarElemento(
            idNodo,
            ordenId,
            operacionId,
            textoComponente,
            numSerie,
            tag,
        )
    }
}
