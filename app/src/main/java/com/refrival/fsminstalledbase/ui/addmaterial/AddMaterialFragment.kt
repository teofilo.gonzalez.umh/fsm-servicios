package com.refrival.fsminstalledbase.ui.addmaterial

import android.Manifest
import android.content.pm.PackageManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.MainViewModel
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_material.*
import java.util.*

@AndroidEntryPoint
class AddMaterialFragment : Base.BaseFragment(), AddMaterialDetailDelegate {

    private lateinit var equipments: MutableList<Equipment>
    private val viewModel: MainViewModel by activityViewModels()
    private val viewModelAddMaterial: AddMaterialViewModel by viewModels()
    private lateinit var ordenId: String
    private lateinit var operacionId: String
    private lateinit var barcode: String
    private lateinit var idPadre: String
    private var nivel: Int = -1

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                navToBarcodeScanner()
            }
        }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_add_material
    }

    override fun init() {
        viewModel.cleanTagId()
        rvEquipments.layoutManager = LinearLayoutManager(activity)
        val addMaterialFragmentArgs by navArgs<AddMaterialFragmentArgs>()
        barcode = addMaterialFragmentArgs.barcode.toString()
        idPadre = addMaterialFragmentArgs.idPadre
        nivel = addMaterialFragmentArgs.nivel
        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!
    }

    override fun initListeners() {
        viewModel.tagId.observe(viewLifecycleOwner) { tagId ->
            if (tagId.isNotEmpty()) {
                serialNumberInput.editText?.setText(tagId.uppercase(Locale.getDefault()))
                viewModel.cleanTagId()
            }
        }

        serialNumberInput.setEndIconOnClickListener {
            showBarcodeScanner()
        }
        editSerialNumberInput.doAfterTextChanged { search ->
            search?.let { filterListEquipment(it.toString()) }
        }

        viewModelAddMaterial.getStocksMontables(ordenId, operacionId, idPadre, nivel)
        viewModelAddMaterial.stocksMontables.observe(viewLifecycleOwner) { stocksMontables ->
            equipments = stocksMontables.map { stock ->
                Equipment(
                    stock.textoMaterial,
                    stock.materialId,
                    "",
                    "",
                    stock.numeroPiezaId,
                    stock.numeroSerieId,
                    stock.materialId
                )
            } as MutableList<Equipment>

            txtMaterialsNumbers.text =
                getString(R.string.header_equipment).plus(" (" + equipments.size.toString() + ")")
            rvEquipments.adapter = EquipmentsAdapter(equipments, this)

            if (barcode != "null") editSerialNumberInput.setText(barcode)
            filterListEquipment(editSerialNumberInput.text.toString())
        }
    }

    private fun filterListEquipment(search: String) {
        equipments.let { list ->
            if (search.isNotEmpty()) {
                val listFiltered: MutableList<Equipment> = list.filter {
                    it.serialNumber.contains(search) || it.tag.contains(search)
                } as MutableList<Equipment>
                if (!listFiltered.isNullOrEmpty()) {
                    txtMaterialsNumbers.text =
                        getString(R.string.header_equipment).plus(" (" + listFiltered.size.toString() + ")")
                    rvEquipments.adapter = EquipmentsAdapter(listFiltered, this)
                } else {
                    listFiltered.clear()
                    txtMaterialsNumbers.text =
                        getString(R.string.header_equipment).plus(" (" + listFiltered.size.toString() + ")")
                    rvEquipments.adapter = EquipmentsAdapter(listFiltered, this)
                }
            } else {
                txtMaterialsNumbers.text =
                    getString(R.string.header_equipment).plus(" (" + equipments.size.toString() + ")")
                rvEquipments.adapter = EquipmentsAdapter(list, this)
            }
        }
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    private fun showBarcodeScanner() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity() as AppCompatActivity,
                Manifest.permission.CAMERA
            ) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            navToBarcodeScanner()
        } else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private fun navToBarcodeScanner() {
        findNavController().navigate(
            AddMaterialFragmentDirections.actionAddMaterialFragmentToBarcodeScannerFragment(
                idPadre = idPadre,
                nivel = nivel
            )
        )
    }

    override fun onItemClick(equipment: Equipment) {
        findNavController().navigate(
            AddMaterialFragmentDirections.actionAddMaterialFragmentToAddMaterialDetailFragment(
                equipment = equipment,
                idPadre = idPadre,
                nivel = nivel
            )
        )
    }
}