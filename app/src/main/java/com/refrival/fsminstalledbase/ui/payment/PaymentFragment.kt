package com.refrival.fsminstalledbase.ui.payment

import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.service.Sipay.AuthenticationResponse
import com.refrival.fsminstalledbase.repository.service.Sipay.SipayApi
import com.refrival.fsminstalledbase.utils.Utils.Companion.createSignature
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import okhttp3.MediaType
import okhttp3.RequestBody
import kotlin.coroutines.CoroutineContext

class PaymentFragment : Base.BaseFragment() {

    override fun getXmlLayout(): Int {
        return R.layout.fragment_payment
    }

    override fun init() {
        webViewSetup()
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
    }

    private fun webViewSetup() {
        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                return if (request.url.toString()
                        .startsWith("https://www.refrival.com/payment/webok.html")
                ) {
                    view.loadUrl("about:blank")
                    val requestId = request.url.getQueryParameter("request_id")
                    findNavController().navigate(
                        PaymentFragmentDirections.actionPaymentFragmentToConfirmPaymentFragment(
                            requestId!!
                        )
                    )
                    true
                } else if (request.url.toString()
                        .startsWith("https://www.refrival.com/payment/webko.html")
                ) {
                    view.loadUrl("about:blank")
                    AlertDialog.Builder(context)
                        .setTitle(resources.getString(R.string.payment_ko))
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                    true
                } else {
                    false
                }
            }
        }

        webview.apply {
            loadUrl("file:///android_asset/sipay/fastpay.html")
            settings.javaScriptEnabled = true
            addJavascriptInterface(WebAppInterface(this.context, resources), "Android")
        }
    }
}

class WebAppInterface(private val mContext: Context, private val resources: Resources) :
    CoroutineScope {
    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    @JavascriptInterface
    fun authenticate(requestId: String): String? {
        val JSON = MediaType.parse("application/json; charset=utf-8")

        val content: String = "{\n" +
                "  \"key\": \"refrival-mdwr-ces\",\n" +
                "  \"resource\": \"refrival-fpay-ces\",\n" +
                "  \"nonce\": \"123456789\",\n" +
                "  \"mode\": \"sha256\",\n" +
                " \"payload\":  {\n" +
                "        \"amount\": \"5\",\n" +
                "        \"currency\": \"EUR\",\n" +
                "        \"operation\": \"authorization\",\n" +
                "        \"url_ok\": \"https://www.refrival.com/payment/webok.html\",\n" +
                "        \"url_ko\": \"https://www.refrival.com/payment/webko.html\",\n" +
                "        \"order\": \"FS000233asdf12346\",\n" +
                "        \"fastpay\": {\n" +
                "            \"request_id\": \"$requestId\"\n" +
                "        }\n" +
                "    }\n" +
                "}";

        val signature: String = createSignature(content, "UfcF6cdDs3")

        val body: RequestBody = RequestBody.create(JSON, content)

        var payloadUrl: String? = ""
        try {
            val response: AuthenticationResponse? =
                SipayApi.retrofitService.authenticate(signature, body).execute().body()
            payloadUrl = response?.payload?.url
        } catch (e: Exception) {
            Toast.makeText(
                mContext,
                resources.getString(R.string.generic_error_call),
                Toast.LENGTH_LONG
            ).show()
        }

        return payloadUrl
    }
}