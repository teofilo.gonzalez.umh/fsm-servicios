package com.refrival.fsminstalledbase.ui.operations

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase

class OperationsViewModel(context: Application, private val owner: LifecycleOwner) : ViewModel() {

    var delegate: OperationsViewModelDelegate? = null
    private var operationDao = RefrivalDatabase.getInstance(context).operacionDao()

    fun getOperations() {
        operationDao.getAll().observe(owner) { operationList ->
            if (operationList != null) delegate?.onGetOperation(operationList)
        }
    }

    fun getOperationsFiltered(todo: Boolean, started: Boolean, completed: Boolean, submitted: Boolean, error: Boolean) {
        operationDao.getOperationFiltered(todo, started, completed, submitted, error).observe(owner) { operationList ->
            if (operationList != null) delegate?.onGetOperationFiltered(operationList)
        }
    }
}