package com.refrival.fsminstalledbase.ui.installedbase

import androidx.lifecycle.*
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.repository.model.BaseInstalada
import com.refrival.fsminstalledbase.repository.model.MotivoCierre
import com.refrival.fsminstalledbase.repository.model.Operacion
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InstalledBaseViewModel
@Inject constructor(
    private val baseInstalledDao: BaseInstaladaDao,
    private val operacionDao: OperacionDao
) : ViewModel() {

    private val _operacion = MutableLiveData<Operacion>()
    val operacion: LiveData<Operacion>
        get() = _operacion

    private val _biCheck = MutableLiveData<List<BaseInstaladaDao.BICheck>?>()
    val biCheck: LiveData<List<BaseInstaladaDao.BICheck>?>
        get() = _biCheck

    fun getOperacion(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            _operacion.value = operacionDao.getById(ordenId, operacionId)
        }
    }

    fun getTreeBaseInstalada(motivoCierre: MotivoCierre): LiveData<List<BIWithLevel>> {
        return baseInstalledDao.getTreeBaseInstalada(
            motivoCierre.ordenId,
            motivoCierre.operacionId,
            motivoCierre.motivoCierre,
            motivoCierre.tipoActividadId,
            motivoCierre.claveModId,
            motivoCierre.etapaLinea
        )
    }

    fun desmontarElemento(elemento: BIWithLevel) {
        val bi = elemento.baseInstalada
        baseInstalledDao.desmontarElemento(bi?.idNodo!!, bi.ordenId, bi.operacionId)
    }

    fun getBICheck(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            _biCheck.value = baseInstalledDao.getBICheck(ordenId, operacionId)
        }
    }

    fun biCheckFinished() {
        _biCheck.value = null
    }

    fun undoAllBIChanges(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            baseInstalledDao.deleteBI(ordenId, operacionId, false)
            val bi = baseInstalledDao.getByOrdenOperacion(ordenId, operacionId, true)
            bi.forEach { item ->
                item.original = false
                baseInstalledDao.insert(item)
            }
        }
    }

    fun seleccionarElemento(elemento: BIWithLevel) {
        val bi = elemento.baseInstalada
        baseInstalledDao.seleccionarElemento(bi?.idNodo!!, bi.ordenId, bi.operacionId)
    }
}