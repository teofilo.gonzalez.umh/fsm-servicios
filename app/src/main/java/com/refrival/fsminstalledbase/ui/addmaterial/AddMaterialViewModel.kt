package com.refrival.fsminstalledbase.ui.addmaterial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.model.Stock
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddMaterialViewModel
@Inject
constructor(
    private val sapRepository: SAPRepository,
    private val operacionDao: OperacionDao
): ViewModel() {

    private val _stocksMontables = MutableLiveData<List<Stock>>()
    val stocksMontables: LiveData<List<Stock>>
        get() = _stocksMontables

    fun getStocksMontables(ordenId: String, operacionId: String, idPadre: String, nivel: Int) {
        viewModelScope.launch {
            val operacion = operacionDao.getById(ordenId, operacionId)
            _stocksMontables.value = sapRepository.getStocksMontables(operacion.motivoCierre!!, idPadre, nivel)
        }
    }
}