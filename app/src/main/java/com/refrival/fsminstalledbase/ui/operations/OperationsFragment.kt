package com.refrival.fsminstalledbase.ui.operations

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.Operacion
import com.refrival.fsminstalledbase.repository.model.enums.OperacionStatus
import com.refrival.fsminstalledbase.utils.CustomViewModelFactory
import kotlinx.android.synthetic.main.fragment_operations.*

class OperationsFragment : Base.BaseFragment(), OperationsViewModelDelegate {

    private var chipRecentChecked = false
    private var chipInProgressChecked = false
    private var chipFinishedChecked = false
    private var chipSendChecked = false
    private var chipNotSendChecked = false

    private val operationsViewModel: OperationsViewModel by lazy {
        val factory = CustomViewModelFactory(requireActivity().application, this)
        ViewModelProvider(this, factory)[OperationsViewModel::class.java]
    }

    private val operationsAdapter: OperationsAdapter by lazy {
        val adapter = OperationsAdapter(requireContext(), this)
        adapter
    }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_operations
    }

    override fun init() {
        operationsViewModel.delegate = this
        filterOperation()
        rvOperations.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rvOperations.adapter = operationsAdapter
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
        chipRecent.setOnCheckedChangeListener { _, checkedId ->
            chipRecentChecked = checkedId
            filterOperation()
        }
        chipInProgress.setOnCheckedChangeListener { _, checkedId ->
            chipInProgressChecked = checkedId
            filterOperation()
        }
        chipFinished.setOnCheckedChangeListener { _, checkedId ->
            chipFinishedChecked = checkedId
            filterOperation()
        }
        chipSend.setOnCheckedChangeListener { _, checkedId ->
            chipSendChecked = checkedId
            filterOperation()
        }
        chipNotSend.setOnCheckedChangeListener { _, checkedId ->
            chipNotSendChecked = checkedId
            filterOperation()
        }
    }

    private fun filterOperation() {
        if (!chipRecentChecked &&
            !chipInProgressChecked &&
            !chipFinishedChecked &&
            !chipSendChecked &&
            !chipNotSendChecked)
                operationsViewModel.getOperations()
        else
            operationsViewModel.getOperationsFiltered(
                chipRecentChecked,
                chipInProgressChecked,
                chipFinishedChecked,
                chipSendChecked,
                chipNotSendChecked
            )
    }

    override fun onGetOperation(operationList: List<Operacion>) {
        operationsAdapter.setResume(operationList)
    }

    override fun onGetOperationFiltered(operationList: List<Operacion>) {
        operationsAdapter.setResume(operationList)
    }

    override fun onItemClick(status: OperacionStatus) {
        when (status) {
            OperacionStatus.TODO -> {
                findNavController().navigate(OperationsFragmentDirections.actionOperationsFragmentToViewInstalledBaseFragment())
            }
            else -> {
                findNavController().navigate(OperationsFragmentDirections.actionOperationsFragmentToInstalledBaseFragment())
            }
        }
    }
}