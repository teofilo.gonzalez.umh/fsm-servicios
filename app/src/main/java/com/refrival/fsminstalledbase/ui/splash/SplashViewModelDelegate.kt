package com.refrival.fsminstalledbase.ui.splash

import com.refrival.fsminstalledbase.repository.model.User


interface SplashViewModelDelegate {
    fun loginSuccess()
    fun loginMissing()
    fun onUserSuccess(user: User)
}