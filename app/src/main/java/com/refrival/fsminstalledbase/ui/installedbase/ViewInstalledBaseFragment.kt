package com.refrival.fsminstalledbase.ui.installedbase

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import com.refrival.fsminstalledbase.utils.PrefManager
import com.refrival.fsminstalledbase.utils.UtilsNetworks
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_installed_base.*

@AndroidEntryPoint
class ViewInstalledBaseFragment : Base.BaseFragment(), HeaderFragment.HeaderDelegate {

    private lateinit var ordenId: String
    private lateinit var operacionId: String

    private val viewInstalledBaseViewModel: ViewInstalledBaseViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_installed_base
    }

    override fun init() {
        rvBaseInstalada.layoutManager = LinearLayoutManager(activity)

        showContentHeader()
        setHasOptionsMenu(true)

        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!
    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun initListeners() {
        (activity as MainActivity).onClickButtonMaterial()

        viewInstalledBaseViewModel.getOperacion(ordenId, operacionId)
        viewInstalledBaseViewModel.operacion.observe(viewLifecycleOwner, { operacion ->
           if (operacion == null) {
               MaterialAlertDialogBuilder(requireContext())
                   .setTitle(R.string.orden_no_encontrada)
                   .setMessage(R.string.message_orden_no_encontrada)
                   .setPositiveButton(resources.getString(R.string.accept)) { _, _ ->
                       findNavController().navigate(ViewInstalledBaseFragmentDirections.actionViewInstalledBaseFragmentToSyncFragment(true))
                   }
                   .setNeutralButton(resources.getString(R.string.cancel)) { _, _ ->
                       (activity as MainActivity).finishAffinity()
                   }
                   .setCancelable(false)
                   .show()
           }
        })

        viewInstalledBaseViewModel.getTreeBaseInstaladaOriginal(ordenId, operacionId).observe(viewLifecycleOwner, { baseInstalada ->
            rvBaseInstalada.adapter = BaseInstaladaAdapter(
                baseInstalada,
                InstalledBaseMode.VIEW,
                BaseInstaladaAdapter.OnClickListener(this::onClickBaseInstalada),
                null
            )
        })
    }

    private fun onClickBaseInstalada(bi: BIWithLevel) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.article_info))
            .setMessage(
                "${resources.getString(R.string.type)}: ${bi.baseInstalada?.tipoComponente}\n" +
                        "${resources.getString(R.string.name)}: ${bi.baseInstalada?.textoComponente}\n" +
                        "${resources.getString(R.string.serial_number)}: ${bi.baseInstalada?.numSerie}\n" +
                        "${resources.getString(R.string.tag)}: ${bi.baseInstalada?.numPiezaFabricante}\n" +
                        "${resources.getString(R.string.status)}: ${bi.baseInstalada?.claveClasificacion}\n" +
                        "${resources.getString(R.string.quantity)}: ${bi.baseInstalada?.cantidad}\n"
            )
            .show()
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.installed_base_menu, menu)

        menu.findItem(R.id.close_installed_base).isVisible = false
        menu.findItem(R.id.undo_changes_installed_base).isVisible = false
        menu.findItem(R.id.regularize).isVisible = true

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.start_installed_base -> {
                viewInstalledBaseViewModel.markStart(ordenId, operacionId)
                findNavController().navigate(ViewInstalledBaseFragmentDirections.actionInstalledBaseFragmentToReasonInstalledBase())
            }
            R.id.regularize -> {
                if (UtilsNetworks.isConnected(requireContext()))
                    findNavController().navigate(ViewInstalledBaseFragmentDirections.actionInstalledBaseFragmentToRegularize())
                else {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(R.string.dialog_title_regularize)
                        .setMessage(R.string.dialog_description_regularize)
                        .show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goToServicePoint() {
        findNavController().navigate(ViewInstalledBaseFragmentDirections.actionInstalledBaseFragmentToServicePointFragment())
    }
}
