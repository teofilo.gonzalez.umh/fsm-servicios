package com.refrival.fsminstalledbase.ui.signatureCheck

import android.app.AlertDialog
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.service.Sign.*
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_signature_check.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SignatureCheckFragment : Base.BaseFragment(), CoroutineScope, MainActivity.IOnBackPressed {
    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    lateinit var idDocument: String

    override fun getXmlLayout(): Int {
        return R.layout.fragment_signature_check
    }

    override fun init() {
        val signatureCheckFragmentArgs by navArgs<SignatureCheckFragmentArgs>()
        idDocument = signatureCheckFragmentArgs.idDocument
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
        buttonContinue.setOnClickListener {
            val dialog: AlertDialog =
                SpotsDialog.Builder().setContext(context).setMessage(R.string.checking_signature)
                    .setCancelable(false).build()
            dialog.show()

            val request = obtenerGetDocumentRequest(idDocument)

            launch {
                val response: GetDocumentResponse?
                try {
                    response = SignApi.retrofitService.getDocument(request).execute().body()
                } catch (e: Exception) {
                    launch(Dispatchers.Main) {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.generic_error_call),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    dialog.dismiss()

                    return@launch
                }
                val code =
                    response?.body?.getDocumentResp?.getDocumentResult?.resultado?.codigoError
                val status = response?.body?.getDocumentResp?.getDocumentResult?.fichero?.idEstado

                dialog.dismiss()

                launch(Dispatchers.Main) {
                    if (code == "0" && status == "6") {
                        Toast.makeText(
                            it.context,
                            resources.getString(R.string.signature_ok),
                            Toast.LENGTH_LONG
                        ).show()

                        findNavController().navigate(SignatureCheckFragmentDirections.actionSignatureCheckFragmentToPaymentFragment())
                    }

                    else if (code != "0") {
                        Toast.makeText(
                            it.context,
                            resources.getString(R.string.error_call_signature),
                            Toast.LENGTH_LONG
                        ).show()
                    }

                    else {
                        Toast.makeText(
                            it.context,
                            resources.getString(R.string.document_is_not_signed),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }

        buttonCancel.setOnClickListener {
            findNavController().navigate(SignatureCheckFragmentDirections.actionSignatureCheckFragmentToBaseInstaladaRegFragment())
        }
    }

    private fun obtenerGetDocumentRequest(idDoc: String): GetDocumentRequest {
        val model = GetDocumentRequestModel(
            codigoEmpresa = "refrival",
            login = "ws58504",
            password = "rMzFsuvrk0",
            idioma = "ES",
            date = "m23o2MHszn128snAkwkaAKnzLK29s91klalzl19Jjkzj19zlxlnwZNmMnN1812nznNMjsjz9MnznWwqsjzlSAK2znqh0z9134",
            idDocumento = idDoc
        )
        val body = GetDocumentRequestBody(model)
        val request = GetDocumentRequest(body)

        return request
    }

    override fun onBackPressed(): Boolean {
        return false
    }
}