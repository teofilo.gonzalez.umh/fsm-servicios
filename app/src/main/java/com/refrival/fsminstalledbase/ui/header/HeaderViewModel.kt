package com.refrival.fsminstalledbase.ui.header

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import com.refrival.fsminstalledbase.utils.PrefManager

class HeaderViewModel(context: Application, private val owner: LifecycleOwner) : ViewModel() {

    var mContext = context
    var delegate: HeaderViewModelDelegate? = null
    private var ordenDao = RefrivalDatabase.getInstance(context).ordenDao()

    fun getEmergencyService() {
        val prefManager = PrefManager(mContext)
        prefManager.ordenId?.let { ordenId ->
            prefManager.operacionId?.let { operacionId ->
                ordenDao.getEmergencyService(ordenId, operacionId)
                    .observe(owner) { servicioEmergencia ->
                        if (servicioEmergencia != null) {
                            if (servicioEmergencia == "")
                                delegate?.setEmergencyService(mContext.getString(R.string.header_service))
                            else delegate?.setEmergencyService(servicioEmergencia)
                        }
                    }
            }
        }
    }

    fun getOutOfTime() {
        val prefManager = PrefManager(mContext)
        prefManager.ordenId?.let { ordenId ->
            prefManager.operacionId?.let { operacionId ->
                ordenDao.getOutOfTime(ordenId, operacionId).observe(owner, { fueraHorario ->
                    if (fueraHorario != null) delegate?.setOutOfTime(fueraHorario)
                })
            }
        }
    }

    fun getDescription() {
        val prefManager = PrefManager(mContext)
        prefManager.ordenId?.let { ordenId ->
            ordenDao.getOrdenDescription(ordenId).observe(owner, { description ->
                if (description != null) delegate?.setDescription(description)
            })
        }
    }
}