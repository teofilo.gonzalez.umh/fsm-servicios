package com.refrival.fsminstalledbase.ui.login

import android.content.Intent
import com.google.firebase.messaging.FirebaseMessaging
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : Base.BaseActivity() {

    override fun getXmlLayout(): Int {
        return R.layout.activity_login
    }

    override fun init() {
        FirebaseMessaging.getInstance().token
        showContentLogin()
    }

    override fun initListeners() {

    }

    private fun showContentLogin() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.contentLogin, LoginFragment())
            .commitNow()
    }

    fun goToMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}