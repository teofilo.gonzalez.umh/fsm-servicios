package com.refrival.fsminstalledbase.ui.closingrequirements

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import kotlinx.android.synthetic.main.item_closing_requirement.view.*

class ClosingRequirementsAdapter(val context: Context) : RecyclerView.Adapter<ClosingRequirementsAdapter.ClosingRequirementHolder>() {

    private val closingRequirements = mutableListOf<String>()

    class ClosingRequirementHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClosingRequirementHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_closing_requirement, parent, false)
        return ClosingRequirementHolder(view)
    }

    override fun onBindViewHolder(holder: ClosingRequirementHolder, position: Int) {
        closingRequirements.let { item ->
            holder.view.imageRequirement.setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.ic_code_scanner_auto_focus_on))
            holder.view.txtName.text = "LECTURA DE TAG"
            holder.view.txtType.text = "Principal"
            holder.view.txtRequirementA.text = "Principal"
            holder.view.txtRequirementB.text = "Auditoría Fotográfica"
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setResume(resume: List<String>) {
        this.closingRequirements.clear()
        this.closingRequirements.addAll(resume)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return closingRequirements.size
    }
}
