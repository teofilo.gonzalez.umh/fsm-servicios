package com.refrival.fsminstalledbase.ui.regularize

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.navigation.fragment.findNavController
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.ui.header.HeaderFragment

class RegularizeFragment : Base.BaseFragment(), HeaderFragment.HeaderDelegate {
    override fun getXmlLayout(): Int {
        return R.layout.fragment_regularize
    }

    override fun init() {
        showContentHeader()
        setHasOptionsMenu(true)
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {

    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.regularize_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.continue_regularize) {
            findNavController().navigate(RegularizeFragmentDirections.actionRegularizeFragmentToBaseInstaladaRegFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goToServicePoint() {
        findNavController().navigate(RegularizeFragmentDirections.actionRegularizeFragmentToServicePointFragment())
    }
}