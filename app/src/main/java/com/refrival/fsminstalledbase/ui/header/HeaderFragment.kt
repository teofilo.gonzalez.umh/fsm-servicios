package com.refrival.fsminstalledbase.ui.header

import androidx.lifecycle.ViewModelProvider
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.CustomViewModelFactory
import kotlinx.android.synthetic.main.fragment_header.*


class HeaderFragment : Base.BaseFragment(), HeaderViewModelDelegate {

    interface HeaderDelegate {
        fun goToServicePoint()
    }

    private val headerViewModel: HeaderViewModel by lazy {
        val factory = CustomViewModelFactory(requireActivity().application, this)
        ViewModelProvider(this, factory)[HeaderViewModel::class.java]
    }

    var delegate: HeaderDelegate? = null

    override fun getXmlLayout(): Int {
        return R.layout.fragment_header
    }

    override fun init() {
        headerViewModel.delegate = this
        headerViewModel.getEmergencyService()
        headerViewModel.getOutOfTime()
        headerViewModel.getDescription()
    }

    override fun showButtonMaterial() {

    }

    override fun initListeners() {
        imageInfo.setOnClickListener {
            delegate?.goToServicePoint()
        }
    }

    override fun setEmergencyService(text: String) {
        txtService.text = text
    }

    override fun setOutOfTime(text: String) {
        txtTimeWork.text = text
    }

    override fun setDescription(text: String) {
        txtMaterial.text = text
    }
}