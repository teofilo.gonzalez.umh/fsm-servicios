package com.refrival.fsminstalledbase.ui.addmaterial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.model.BaseInstalada
import com.refrival.fsminstalledbase.utils.ACCION_BI_MONTAR
import com.refrival.fsminstalledbase.utils.CLAVE_CLASIFICACION_ACTIVO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class AddMaterialDetailViewModel
@Inject constructor(
    private val baseInstaladaDao: BaseInstaladaDao,
) : ViewModel() {

    private val _navigateToInstalledBase = MutableLiveData<Boolean?>()
    val navigateToInstalledBase: LiveData<Boolean?>
        get() = _navigateToInstalledBase

    fun montarEquipo(
        ordenId: String,
        operacionId: String,
        equipment: Equipment,
        idPadre: String,
        materialCount: Int,
        nivel: Int
    ) {
        viewModelScope.launch {
            var idPadreCorrecta = idPadre
            if (nivel == 0) {
                val root = baseInstaladaDao.getRoot(ordenId, operacionId, false)
                idPadreCorrecta = root.idNodo
            }

            baseInstaladaDao.insert(
                BaseInstalada(
                    ordenId = ordenId,
                    operacionId = operacionId,
                    idNodo = UUID.randomUUID().toString(),
                    original = false,
                    accion = ACCION_BI_MONTAR,
                    componente = equipment.materialId,
                    textoComponente = equipment.name,
                    numPiezaFabricante = equipment.tag,
                    numSerie = equipment.serialNumber,
                    idPadre = idPadreCorrecta,
                    tipoComponente = "TODO",
                    cantidad = materialCount,
                    claveClasificacion = CLAVE_CLASIFICACION_ACTIVO,
                )
            )

            _navigateToInstalledBase.value = true
        }
    }

    fun navigateToInstalledBaseCompleted() {
        _navigateToInstalledBase.value = null
    }
}