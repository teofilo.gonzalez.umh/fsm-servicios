package com.refrival.fsminstalledbase.ui.header



interface HeaderViewModelDelegate {
    fun setEmergencyService(text: String)
    fun setOutOfTime(text: String)
    fun setDescription(text: String)
}