package com.refrival.fsminstalledbase.ui.addmaterial

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_material_detail.*

@AndroidEntryPoint
class AddMaterialDetailFragment : Base.BaseFragment() {

    private var materialCount: Int = 1
    private lateinit var equipment: Equipment
    private lateinit var ordenId: String
    private lateinit var operacionId: String
    private lateinit var idPadre: String
    private var nivel: Int = -1

    private val viewModel: AddMaterialDetailViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_add_material_detail
    }

    override fun init() {
        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!

        setHasOptionsMenu(true)
        textInputEditAddMaterial.setText(materialCount.toString())
        val addMaterialDetailFragmentArgs by navArgs<AddMaterialDetailFragmentArgs>()
        equipment = addMaterialDetailFragmentArgs.equipment
        idPadre = addMaterialDetailFragmentArgs.idPadre
        nivel = addMaterialDetailFragmentArgs.nivel
        txtEquipmentName.text = addMaterialDetailFragmentArgs.equipment.name
        txtEquipmentID.text = addMaterialDetailFragmentArgs.equipment.id
        txtEquipmentDescription.text = addMaterialDetailFragmentArgs.equipment.description
        txtEquipmentModel.text = addMaterialDetailFragmentArgs.equipment.model
        txtEquipmentTag.text = addMaterialDetailFragmentArgs.equipment.tag
        txtEquipmentSerialNumber.text = addMaterialDetailFragmentArgs.equipment.serialNumber
    }

    override fun initListeners() {
        imageAddEquipment.setOnClickListener {
            materialCount++
            textInputEditAddMaterial.setText(materialCount.toString())
        }
        imageRemoveEquipment.setOnClickListener {
            if (materialCount > 1) {
                materialCount--
                textInputEditAddMaterial.setText(materialCount.toString())
            }
        }

        viewModel.navigateToInstalledBase.observe(viewLifecycleOwner) {
            if (it == true) {
                findNavController().navigate(
                    AddMaterialDetailFragmentDirections.actionAddMaterialDetailFragmentToInstalledBaseFragment()
                )
                viewModel.navigateToInstalledBaseCompleted()
            }
        }
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.add_material_detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.assembly_equipment) {
            montarEquipo()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun montarEquipo() {
        viewModel.montarEquipo(ordenId, operacionId, equipment, idPadre, materialCount, nivel)
    }
}