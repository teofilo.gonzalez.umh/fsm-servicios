package com.refrival.fsminstalledbase.ui.confirmpayment

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.repository.service.Sipay.ConfirmAuthenticationResponse
import com.refrival.fsminstalledbase.repository.service.Sipay.SipayApi
import com.refrival.fsminstalledbase.utils.Utils.Companion.createSignature
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import kotlin.coroutines.CoroutineContext


class ConfirmPaymentFragment : Fragment(), CoroutineScope {
    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val confirmPaymentFragmentArgs by navArgs<ConfirmPaymentFragmentArgs>()
        confirmAuthentication(confirmPaymentFragmentArgs.requestId)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_payment, container, false)
    }

    private fun confirmAuthentication(requestId: String) {
        val JSON = MediaType.parse("application/json; charset=utf-8")

        val content: String = "{\n" +
                "  \"key\": \"refrival-mdwr-ces\",\n" +
                "  \"resource\": \"refrival-fpay-ces\",\n" +
                "  \"nonce\": \"123456789\",\n" +
                "  \"mode\": \"sha256\",\n" +
                " \"payload\": {\n" +
                "        \"request_id\": \"$requestId\"\n" +
                "    }\n" +
                "}";

        val signature: String = createSignature(content, "UfcF6cdDs3")

        val body: RequestBody = RequestBody.create(JSON, content)

        launch {
            try {
                val response: ConfirmAuthenticationResponse? =
                    SipayApi.retrofitService.confirmAuthentication(signature, body).execute().body()

                launch(Dispatchers.Main) {
                    if (response?.code == "0") {
                        AlertDialog.Builder(context)
                            .setTitle(resources.getString(R.string.payment_ok))
                            .setPositiveButton(android.R.string.ok
                            ) { _, _ ->
                                findNavController().navigate(ConfirmPaymentFragmentDirections.actionConfirmPaymentFragmentToOperationsFragment())
                            }
                            .show()
                    } else {
                        AlertDialog.Builder(context)
                            .setTitle(resources.getString(R.string.payment_ko))
                            .setPositiveButton(android.R.string.ok, null)
                            .show()
                    }
                }
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.generic_error_call),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}