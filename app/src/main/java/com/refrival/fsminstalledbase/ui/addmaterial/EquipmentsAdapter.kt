package com.refrival.fsminstalledbase.ui.addmaterial

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import kotlinx.android.synthetic.main.item_equipment.view.*

class EquipmentsAdapter (private val list: List<Equipment>, private val addMaterialDetailDelegate: AddMaterialDetailDelegate) : RecyclerView.Adapter<EquipmentsAdapter.EquipmentHolder>() {

    class EquipmentHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EquipmentHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_equipment, parent, false)
        return EquipmentHolder(view)
    }

    override fun onBindViewHolder(holder: EquipmentHolder, position: Int) {
        list.let { item ->
            holder.view.txtMaterialName.text = item[position].name
            holder.view.textMaterialID.text = item[position].id
            holder.view.txtMaterialTag.text = item[position].tag
            holder.view.txtMaterialNumberSerial.text = item[position].serialNumber
            holder.view.lyEquipment.setOnClickListener {
                addMaterialDetailDelegate.onItemClick(item[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}