package com.refrival.fsminstalledbase.ui.modificardatos

import android.Manifest
import android.content.pm.PackageManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.MainViewModel
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_modificar_datos.*
import java.util.*

@AndroidEntryPoint
class ModificarDatosFragment : Base.BaseFragment() {

    private val activityViewModel: MainViewModel by activityViewModels()
    private val viewModel: ModificarDatosViewModel by viewModels()

    private lateinit var ordenId: String
    private lateinit var operacionId: String
    private lateinit var idNodo: String

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                navToBarcodeScanner()
            }
        }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_modificar_datos
    }

    override fun init() {
        activityViewModel.cleanTagId()

        setHasOptionsMenu(true)

        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!

        val modificarDatosFragmentArgs by navArgs<ModificarDatosFragmentArgs>()

        idNodo = modificarDatosFragmentArgs.idNodo

        textoComponente.text = modificarDatosFragmentArgs.textoComponenteOriginal
        numSerie.text = modificarDatosFragmentArgs.numSerieOriginal
        tagValue.text = modificarDatosFragmentArgs.tagOriginal
        textoComponenteEdit.editText?.setText(modificarDatosFragmentArgs.textoComponente)
        numSerieInput.editText?.setText(modificarDatosFragmentArgs.numSerie)
        tagInput.editText?.setText(modificarDatosFragmentArgs.tag)
    }

    override fun initListeners() {
        activityViewModel.tagId.observe(viewLifecycleOwner) { tagId ->
            if (tagId.isNotEmpty()) {
                tagInput.editText?.setText(tagId.uppercase(Locale.getDefault()))
                activityViewModel.cleanTagId()
            }
        }

        numSerieInput.setEndIconOnClickListener {
            showBarcodeScanner()
        }
    }

    private fun showBarcodeScanner() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity() as AppCompatActivity,
                Manifest.permission.CAMERA
            ) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            navToBarcodeScanner()
        } else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private fun navToBarcodeScanner() {
        findNavController().navigate(
            ModificarDatosFragmentDirections.actionModificarDatosFragmentToBarcodeScannerModifyFragment(
                idNodo = idNodo,
                textoComponente = textoComponenteEdit.editText?.text.toString(),
                tag = tagInput.editText?.text.toString(),
                textoComponenteOriginal = textoComponente.text.toString(),
                numSerieOriginal = numSerie.text.toString(),
                tagOriginal = tagValue.text.toString()
            )
        )
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.modificar_datos_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.accept -> {
                viewModel.modificarElemento(
                    idNodo,
                    ordenId,
                    operacionId,
                    textoComponenteEdit.editText?.text.toString(),
                    numSerieInput.editText?.text.toString(),
                    tagInput.editText?.text.toString()
                )
                findNavController().navigate(ModificarDatosFragmentDirections.actionModificarDatosFragmentToBaseInstaladaRegFragment())
            }
            R.id.cancel -> findNavController().navigate(ModificarDatosFragmentDirections.actionModificarDatosFragmentToBaseInstaladaRegFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onAceptarCambios() {

    }
}