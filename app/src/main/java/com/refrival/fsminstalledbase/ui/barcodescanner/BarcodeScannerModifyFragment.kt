package com.refrival.fsminstalledbase.ui.barcodescanner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.refrival.fsminstalledbase.R

class BarcodeScannerModifyFragment : Fragment() {
    private lateinit var codeScanner: CodeScanner
    private lateinit var idNodo: String
    private lateinit var textoComponente: String
    private lateinit var tagValue: String
    private lateinit var textoComponenteOriginal: String
    private lateinit var numSerieOriginal: String
    private lateinit var tagOriginal: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_barcode_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val barcodeScannerModifyFragmentArgs by navArgs<BarcodeScannerModifyFragmentArgs>()
        idNodo = barcodeScannerModifyFragmentArgs.idNodo
        textoComponente = barcodeScannerModifyFragmentArgs.textoComponente
        tagValue = barcodeScannerModifyFragmentArgs.tag
        textoComponenteOriginal = barcodeScannerModifyFragmentArgs.textoComponenteOriginal
        numSerieOriginal = barcodeScannerModifyFragmentArgs.numSerieOriginal
        tagOriginal = barcodeScannerModifyFragmentArgs.tagOriginal

        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.decodeCallback = DecodeCallback {
            activity.runOnUiThread {
                val action =
                    BarcodeScannerModifyFragmentDirections.actionBarcodeScannerModifyFragmentToModificarDatosFragment(
                        idNodo = idNodo,
                        textoComponente = textoComponente,
                        numSerie = it.text,
                        tag = tagValue,
                        textoComponenteOriginal = textoComponenteOriginal,
                        numSerieOriginal = numSerieOriginal,
                        tagOriginal = tagOriginal
                    )
                findNavController().navigate(action)
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}