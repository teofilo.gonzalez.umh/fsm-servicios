package com.refrival.fsminstalledbase.ui.operations

import com.refrival.fsminstalledbase.repository.model.Operacion
import com.refrival.fsminstalledbase.repository.model.enums.OperacionStatus


interface OperationsViewModelDelegate {
    fun onGetOperation(operationList: List<Operacion>)
    fun onGetOperationFiltered(operationList: List<Operacion>)
    fun onItemClick(status: OperacionStatus)
}