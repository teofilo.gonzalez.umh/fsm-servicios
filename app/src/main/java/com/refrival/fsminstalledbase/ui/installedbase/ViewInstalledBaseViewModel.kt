package com.refrival.fsminstalledbase.ui.installedbase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.repository.model.Operacion
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ViewInstalledBaseViewModel
@Inject constructor(private val baseInstalledDao: BaseInstaladaDao, private val operacionDao: OperacionDao) : ViewModel() {

    private val _operacion = MutableLiveData<Operacion>()
    val operacion: LiveData<Operacion>
        get() = _operacion

    fun markStart(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            operacionDao.markStart(ordenId, operacionId)
        }
    }

    fun getTreeBaseInstaladaOriginal(ordenId: String, operacionId: String): LiveData<List<BIWithLevel>> {
        return baseInstalledDao.getTreeBaseInstaladaOriginal(ordenId, operacionId)
    }

    fun getOperacion(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            _operacion.value = operacionDao.getById(ordenId, operacionId)
        }
    }
}