package com.refrival.fsminstalledbase.ui.operations

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.repository.model.Operacion
import com.refrival.fsminstalledbase.repository.model.enums.OperacionStatus
import com.refrival.fsminstalledbase.utils.PrefManager
import kotlinx.android.synthetic.main.item_operations.view.*

class OperationsAdapter(val context: Context, private val operationsDelegate: OperationsViewModelDelegate) : RecyclerView.Adapter<OperationsAdapter.OperationsHolder>() {

    private val operationsList = mutableListOf<Operacion>()

    class OperationsHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OperationsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_operations, parent, false)
        return OperationsHolder(view)
    }

    override fun onBindViewHolder(holder: OperationsHolder, position: Int) {
        operationsList.let { item ->
            holder.view.imageAssembly.setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.ic_operations_item))
            holder.view.txtAssembly.text = item[position].claveModDesc
            holder.view.txtAssemblyId.text = context.getString(R.string.operation_id).plus(item[position].operacionId)
            holder.view.txtAssemblyDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
            setStatus(item[position].status, holder)
            holder.view.btnAssembly.setOnClickListener {
                PrefManager(context).saveOperacionId(item[position].operacionId)
                PrefManager(context).saveOrdenId(item[position].ordenId)
                operationsDelegate.onItemClick(item[position].status)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setResume(resume: List<Operacion>) {
        this.operationsList.clear()
        this.operationsList.addAll(resume)
        notifyDataSetChanged()
    }

    private fun setStatus(status: OperacionStatus, holder: OperationsHolder) {
        when (status) {
            OperacionStatus.TODO -> {
                holder.view.txtAssemblyStatus.text = context.getString(R.string.operation_todo)
                holder.view.txtAssemblyStatus.background = ContextCompat.getDrawable(context, R.drawable.layout_inactive_bg)
            }
            OperacionStatus.STARTED -> {
                holder.view.txtAssemblyStatus.text = context.getString(R.string.operation_in_progress)
                holder.view.txtAssemblyStatus.background = ContextCompat.getDrawable(context, R.drawable.layout_warning_bg)
            }
            OperacionStatus.COMPLETED -> {
                holder.view.txtAssemblyStatus.text = context.getString(R.string.operation_finished)
                holder.view.txtAssemblyStatus.background = ContextCompat.getDrawable(context, R.drawable.layout_finished_bg)
            }
            OperacionStatus.SUBMITTED -> {
                holder.view.txtAssemblyStatus.text = context.getString(R.string.operation_send)
                holder.view.txtAssemblyStatus.background = ContextCompat.getDrawable(context, R.drawable.layout_active_bg)
            }
            OperacionStatus.ERROR -> {
                holder.view.txtAssemblyStatus.text = context.getString(R.string.operation_error)
                holder.view.txtAssemblyStatus.background = ContextCompat.getDrawable(context, R.drawable.layout_error_bg)
            }
        }
    }

    override fun getItemCount(): Int {
        return operationsList.size
    }
}