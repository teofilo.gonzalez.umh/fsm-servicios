package com.refrival.fsminstalledbase.ui.installedbase

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.utils.ACCION_BI_DESMONTAR
import com.refrival.fsminstalledbase.utils.ACCION_BI_MONTAR
import com.refrival.fsminstalledbase.utils.ACCION_BI_SELECCIONAR
import kotlinx.android.synthetic.main.item_base_instalada.view.*

class BaseInstaladaAdapter(private val list: List<BIWithLevel>, private val mode: InstalledBaseMode, val onClickListener: OnClickListener, val onLongClickListener: OnLongClickListener?) : RecyclerView.Adapter<BaseInstaladaAdapter.BaseInstaladaHolder>()  {
    class BaseInstaladaHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseInstaladaHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_base_instalada, parent, false)
        return BaseInstaladaHolder(view)
    }

    override fun onBindViewHolder(holder: BaseInstaladaHolder, position: Int) {
        list.let { item ->
            val bi = item[position]
            setupIndent(holder, bi)

            setTypeComponent(bi.baseInstalada?.tipoComponente, holder)
            holder.view.name.text = bi.baseInstalada?.textoComponente
            holder.view.quantity.text = bi.baseInstalada?.cantidad?.toDouble()?.toInt().toString()
            holder.view.code.text = bi.baseInstalada?.numSerie
            setRemovable(bi.esDesmontable, holder)
            setClassification(bi.baseInstalada?.claveClasificacion, holder)

            when (bi.baseInstalada?.accion) {
                ACCION_BI_DESMONTAR -> holder.view.container.setBackgroundColor(Color.parseColor("#ffcccb"))
                ACCION_BI_MONTAR -> holder.view.container.setBackgroundColor(Color.parseColor("#d2f8d2"))
                ACCION_BI_SELECCIONAR -> holder.view.container.setBackgroundColor(Color.parseColor("#d4ebf2"))
                else -> {
                    holder.view.container.setBackgroundColor(Color.TRANSPARENT)
                }
            }

            holder.view.container.setOnClickListener {
                onClickListener.onClick(bi)
            }

            if (mode == InstalledBaseMode.EDIT) {
                holder.view.container.setOnLongClickListener {
                    onLongClickListener!!.onLongClick(bi)
                }
            }
        }
    }

    private fun setupIndent(holder: BaseInstaladaHolder, bi: BIWithLevel) {
        val container = holder.view.indentContainer

        val level = bi.level - 2

        when {
            container.childCount < level -> { // More levels needed
                // Make all existing levels visible
                for (i in 1..container.childCount) {
                    container.getChildAt(i - 1).visibility = View.VISIBLE
                }

                // Inflate the rest
                for (i in container.childCount + 1..level) {
                    View.inflate(container.context, R.layout.indent, container)
                }
            }

            level < container.childCount -> { // Too many levels
                // Make required levels visible
                for (i in 1..level) {
                    container.getChildAt(i - 1).visibility = View.VISIBLE
                }

                // Hide the rest
                for (i in level + 1..container.childCount) {
                    container.getChildAt(i - 1).visibility = View.GONE
                }
            }

            else -> // Make all visible
                for (i in 1..container.childCount) {
                    container.getChildAt(i - 1).visibility = View.VISIBLE
                }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class OnClickListener(val clickListener: (baseInstalada: BIWithLevel) -> Unit) {
        fun onClick(baseInstalada:BIWithLevel) = clickListener(baseInstalada)
    }

    class OnLongClickListener(val longClickListener: (baseInstalada: BIWithLevel) -> Boolean) {
        fun onLongClick(baseInstalada:BIWithLevel) = longClickListener(baseInstalada)
    }

    private fun setTypeComponent(typeComponent: String?, holder: BaseInstaladaHolder) {
        when (typeComponent) {
            "0002" -> {
                holder.view.icon.text = "M"
            }
            "0005" -> {
                holder.view.icon.text = "S"
            }
            "0003" -> {
                holder.view.icon.text = "E"
            }
            else -> {
                holder.view.icon.text = "R"
            }
        }
    }

    private fun setRemovable(removable: Boolean, holder: BaseInstaladaHolder) {
        holder.view.txtDesmontable.visibility = if (removable) View.VISIBLE else View.GONE
    }

    private fun setClassification(classification: String?, holder: BaseInstaladaHolder) {
        if (classification == "INACTIVO") {
            holder.view.txtInActive.visibility = View.VISIBLE
            holder.view.txtActive.visibility = View.GONE
        } else {
            holder.view.txtActive.visibility = View.VISIBLE
            holder.view.txtInActive.visibility = View.GONE
        }
    }
}

enum class InstalledBaseMode {
    VIEW, EDIT
}
