package com.refrival.fsminstalledbase.ui.closingrequirements

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.CustomViewModelFactory
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import kotlinx.android.synthetic.main.fragment_closing_requirements.*


class ClosingRequirementsFragment : Base.BaseFragment(),
    ClosingRequirementViewModelDelegate, HeaderFragment.HeaderDelegate {

    private val closingRequirementsViewModel: ClosingRequirementsViewModel by lazy {
        val factory = CustomViewModelFactory(requireActivity().application, this)
        ViewModelProvider(this, factory)[ClosingRequirementsViewModel::class.java]
    }

    private val closingRequirementsAdapter: ClosingRequirementsAdapter by lazy {
        val adapter = ClosingRequirementsAdapter(requireContext())
        adapter
    }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_closing_requirements
    }

    override fun init() {
        closingRequirementsViewModel.delegate = this
        closingRequirementsViewModel.init()
    }

    override fun initialize() {
        showContentHeader()
        setHasOptionsMenu(true)

        rvClosingRequirements.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rvClosingRequirements.adapter = closingRequirementsAdapter
        closingRequirementsAdapter.setResume(arrayListOf("1", "2", "3", "4"))
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {

    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.closing_requirements_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.close_service_order) {
            findNavController().navigate(ClosingRequirementsFragmentDirections.actionClosingRequirementsFragmentToClosingConfirmationFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goToServicePoint() {
        findNavController().navigate(ClosingRequirementsFragmentDirections.actionClosingRequirementsFragmentToServicePointFragment())
    }
}