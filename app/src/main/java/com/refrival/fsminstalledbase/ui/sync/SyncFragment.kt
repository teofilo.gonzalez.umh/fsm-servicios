package com.refrival.fsminstalledbase.ui.sync

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.work.*
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.sync.PeriodicWorker
import com.refrival.fsminstalledbase.utils.PrefManager
import com.refrival.fsminstalledbase.utils.UNIQUE_SYNC_WORK_NAME
import com.refrival.fsminstalledbase.utils.UtilsNetworks
import kotlinx.android.synthetic.main.fragment_sync.*

class SyncFragment : Base.BaseFragment() {

    private val viewModel: SyncViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_sync
    }

    override fun init() {
        if (!UtilsNetworks.isConnected(requireContext())) {
            navToInstalledBase()
            return
        }

        val syncFragmentArgs by navArgs<SyncFragmentArgs>()

        PeriodicWorker.enqueue(requireContext())

        if (PrefManager(requireContext()).isFirstSync || syncFragmentArgs.show) {
            observeUi()
            PrefManager(requireContext()).saveIsFirstSync(false)
        } else {
            navToInstalledBase()
            return
        }
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {

    }

    private fun observeUi() {
        viewModel.progress.observe(viewLifecycleOwner, { i ->
            syncProgressBar.progress = i
        })

        WorkManager.getInstance(requireContext()).pruneWork()
        WorkManager.getInstance(requireContext())
            .getWorkInfosForUniqueWorkLiveData(UNIQUE_SYNC_WORK_NAME)
            .observe(viewLifecycleOwner, { workInfos ->
                if (workInfos.size == 0) return@observe
                val workInfo = workInfos[0]
                if (workInfo != null && workInfo.state == WorkInfo.State.SUCCEEDED) {
                    viewModel.setProgress(100)
                    navToInstalledBase()
                }
                else if (workInfo != null && workInfo.state == WorkInfo.State.ENQUEUED) {
                    viewModel.setProgress(0)
                }
                else if (workInfo != null && workInfo.state == WorkInfo.State.RUNNING) {
                    val progress: Int = workInfo.progress.getInt("progress", 0)
                    viewModel.setProgress(progress)
                }
                else if (workInfo != null && workInfo.state == WorkInfo.State.FAILED) {
                    // TODO
                }
            })
    }

    private fun navToInstalledBase() {
        findNavController().navigate(SyncFragmentDirections.actionSyncFragmentToViewInstalledBaseFragment())
    }
}