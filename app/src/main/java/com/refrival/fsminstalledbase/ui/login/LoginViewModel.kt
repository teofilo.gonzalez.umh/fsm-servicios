package com.refrival.fsminstalledbase.ui.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import com.refrival.fsminstalledbase.repository.model.User
import com.refrival.fsminstalledbase.repository.model.UserRequest
import com.refrival.fsminstalledbase.repository.service.fsm.FSMRepository
import com.refrival.fsminstalledbase.repository.service.fsm.FSMService
import com.refrival.fsminstalledbase.repository.service.fsm.FSMServiceImpl
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import com.refrival.fsminstalledbase.repository.service.sap.SAPService
import com.refrival.fsminstalledbase.utils.Crypt
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
@Inject
    constructor(
        refrivalDatabase: RefrivalDatabase,
        private val sapRepository: SAPRepository
    ): ViewModel() {

    var delegate: LoginViewModelDelegate? = null
    private var fsmRepository: FSMRepository = FSMServiceImpl()
    private var userRepository = refrivalDatabase.userDao()

    fun init() {
        delegate?.initialize()
    }

    fun login(userName: String, userPassword: String) : MutableLiveData<User?> {
        val liveData : MutableLiveData<User?> = MutableLiveData<User?>()
        fsmRepository.login(userName, userPassword, object : FSMService.CallbackResponse<User>{
            override fun onResponse(response: User) {

                // Insert user into local DB
                response.password = Crypt.encryptAndEncode(userPassword)
                response.name = Crypt.encryptAndEncode("${response.account}/${response.user}")
                insertUser(response)

                liveData.value = response
            }

            override fun onFailure(t: Throwable, res: Response<*>?) {
                liveData.value = null
            }
        })
        return liveData
    }

    fun insertUser(user: User) {
        userRepository.insert(user)
    }

    fun sentUserAndToken(userRequest: UserRequest) {
        sapRepository.sendUserAndToken(userRequest, object : SAPService.CallbackResponse<String>{
            override fun onResponse(response: String) {
                Log.w("TAG", "USER-TOKEN: $response")
            }

            override fun onFailure(t: Throwable, res: Response<*>?) {
                Log.e("TAG", "USER-TOKEN - Error: ${t.localizedMessage}")
            }
        })
    }
}