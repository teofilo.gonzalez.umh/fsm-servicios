package com.refrival.fsminstalledbase.ui.servicepoint

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.navigation.fragment.findNavController
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.ui.closingconfirmation.ClosingConfirmationFragmentDirections

class ServicePointFragment : Base.BaseFragment() {
    override fun getXmlLayout(): Int {
        return R.layout.fragment_service_point
    }

    override fun init() {
        setHasOptionsMenu(true)
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.point_service_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.cancel_service_point) {
            activity?.onBackPressed()
        }
        if (item.itemId == R.id.save_service_point) {
            findNavController().navigate(ClosingConfirmationFragmentDirections.actionClosingConfirmationFragmentToSignatureFragment())
        }
        return super.onOptionsItemSelected(item)
    }
}