package com.refrival.fsminstalledbase.ui.closingconfirmation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.model.BaseInstalada
import com.refrival.fsminstalledbase.repository.model.Operacion
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ClosingConfirmationViewModel
@Inject constructor(
    private val baseInstalledDao: BaseInstaladaDao,
    private val operacionDao: OperacionDao
) : ViewModel() {

    private val _baseInstalada = MutableLiveData<List<BaseInstalada>?>()
    val baseInstalada: LiveData<List<BaseInstalada>?>
        get() = _baseInstalada

    private val _operation = MutableLiveData<Operacion>()
    val operation: LiveData<Operacion>
        get() = _operation

    fun getComponentsEdited() {
        viewModelScope.launch {
            _baseInstalada.value = baseInstalledDao.getComponenteEditado()
        }
    }

    fun getClosingConfirmationResult(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            _operation.value = operacionDao.getById(ordenId, operacionId)
        }
    }
}