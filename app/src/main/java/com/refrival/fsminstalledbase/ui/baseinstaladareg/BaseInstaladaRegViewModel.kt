package com.refrival.fsminstalledbase.ui.baseinstaladareg

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.BaseInstaladaDao
import com.refrival.fsminstalledbase.repository.model.BIReg
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BaseInstaladaRegViewModel
@Inject constructor(
    private val baseInstalledDao: BaseInstaladaDao,
) : ViewModel() {

    fun getTreeBaseInstalada(ordenId: String, operacionId: String): LiveData<List<BIReg>> {
        return baseInstalledDao.getTreeBaseInstaladaReg(ordenId, operacionId)
    }

}