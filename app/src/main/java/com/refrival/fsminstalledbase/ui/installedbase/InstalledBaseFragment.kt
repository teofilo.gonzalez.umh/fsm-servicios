package com.refrival.fsminstalledbase.ui.installedbase

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_installed_base.*

@AndroidEntryPoint
class InstalledBaseFragment : Base.BaseFragment(), HeaderFragment.HeaderDelegate {

    private lateinit var ordenId: String
    private lateinit var operacionId: String
    private var installedBase : List<BIWithLevel>? = null
    private val installedBaseViewModel: InstalledBaseViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_installed_base
    }

    override fun init() {
        rvBaseInstalada.layoutManager = LinearLayoutManager(activity)

        showContentHeader()
        setHasOptionsMenu(true)

        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!
    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun initListeners() {
        (activity as MainActivity).onClickButtonMaterial()

        installedBaseViewModel.getOperacion(ordenId, operacionId)
        installedBaseViewModel.operacion.observe(viewLifecycleOwner) { operacion ->
            installedBaseViewModel.getTreeBaseInstalada(operacion.motivoCierre!!)
                .observe(viewLifecycleOwner) { baseInstalada ->
                    installedBase = baseInstalada
                    rvBaseInstalada.adapter = BaseInstaladaAdapter(
                        baseInstalada,
                        InstalledBaseMode.EDIT,
                        BaseInstaladaAdapter.OnClickListener(this::onClickBaseInstalada),
                        BaseInstaladaAdapter.OnLongClickListener(this::onLongClickBaseInstalada)
                    )
                }
        }

        installedBaseViewModel.biCheck.observe(viewLifecycleOwner) { listBiCheck ->
            if (listBiCheck == null) {
                return@observe
            }
            if (listBiCheck.isEmpty()) {
                installedBaseViewModel.biCheckFinished()
                findNavController().navigate(InstalledBaseFragmentDirections.actionInstalledBaseFragmentToClosingRequirementsFragment())
                return@observe
            }

            val biCheck = listBiCheck[0]

            val message: String = if (biCheck.esNivelMenos1) {
                if (biCheck.nodosMaximos == biCheck.nodosMinimos) {
                    resources.getString(
                        R.string.msg_bi_check_equal_nivel_menos_1,
                        biCheck.nodosMinimos
                    )
                } else {
                    resources.getString(
                        R.string.msg_bi_check_nivel_menos_1,
                        biCheck.nodosMinimos,
                        biCheck.nodosMaximos
                    )
                }
            } else {
                if (biCheck.nodosMaximos == biCheck.nodosMinimos) {
                    resources.getString(
                        R.string.msg_bi_check_equal,
                        biCheck.textoComponente,
                        biCheck.nodosMinimos
                    )
                } else {
                    resources.getString(
                        R.string.msg_bi_check,
                        biCheck.textoComponente,
                        biCheck.nodosMinimos,
                        biCheck.nodosMaximos
                    )
                }
            }


            MaterialAlertDialogBuilder(requireContext())
                .setTitle(resources.getString(R.string.msg_bi_check_title))
                .setMessage(message)
                .show()

            installedBaseViewModel.biCheckFinished()
        }
    }

    private fun onLongClickBaseInstalada(bi: BIWithLevel): Boolean {
        val actions: MutableList<String> = mutableListOf()
        actions.add(resources.getString(R.string.accion_montar))
        if (bi.esDesmontable) {
            actions.add(resources.getString(R.string.accion_desmontar))
        }
        actions.add(resources.getString(R.string.accion_reportar_averia))
        actions.add(resources.getString(R.string.accion_seleccionar))

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("${bi.baseInstalada?.textoComponente}")
            .setItems(actions.toTypedArray()) { dialog, which ->
                when (actions[which]) {
                    resources.getString(R.string.accion_montar) -> findNavController().navigate(
                        InstalledBaseFragmentDirections.actionInstalledBaseFragmentToAddMaterialFragment(
                            idPadre = bi.baseInstalada?.idNodo!!,
                            nivel = bi.level + 1
                        )
                    )
                    resources.getString(R.string.accion_desmontar) -> installedBaseViewModel.desmontarElemento(bi)
                    resources.getString(R.string.accion_reportar_averia) -> findNavController().navigate(InstalledBaseFragmentDirections.actionInstalledBaseFragmentToFaultCausesFragment())
                    resources.getString(R.string.accion_seleccionar) -> installedBaseViewModel.seleccionarElemento(bi)
                }
            }
            .show()

        return true
    }

    private fun onClickBaseInstalada(bi: BIWithLevel) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.article_info))
            .setMessage(
                "${resources.getString(R.string.type)}: ${bi.baseInstalada?.tipoComponente}\n" +
                        "${resources.getString(R.string.name)}: ${bi.baseInstalada?.textoComponente}\n" +
                        "${resources.getString(R.string.serial_number)}: ${bi.baseInstalada?.numSerie}\n" +
                        "${resources.getString(R.string.tag)}: ${bi.baseInstalada?.numPiezaFabricante}\n" +
                        "${resources.getString(R.string.status)}: ${bi.baseInstalada?.claveClasificacion}\n" +
                        "${resources.getString(R.string.quantity)}: ${bi.baseInstalada?.cantidad}\n"
            )
            .show()
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.installed_base_menu, menu)

        menu.findItem(R.id.close_installed_base).isVisible = true
        menu.findItem(R.id.undo_changes_installed_base).isVisible = true
        menu.findItem(R.id.start_installed_base).isVisible = false
        menu.findItem(R.id.regularize).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.close_installed_base -> {
                onFinalizar()
            }
            R.id.undo_changes_installed_base -> {
                installedBaseViewModel.undoAllBIChanges(ordenId, operacionId)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onFinalizar() {
        installedBaseViewModel.getBICheck(ordenId, operacionId)
    }

    override fun goToServicePoint() {
        findNavController().navigate(InstalledBaseFragmentDirections.actionInstalledBaseFragmentToServicePointFragment())
    }
}