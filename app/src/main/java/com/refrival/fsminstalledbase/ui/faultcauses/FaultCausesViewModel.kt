package com.refrival.fsminstalledbase.ui.faultcauses

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel

class FaultCausesViewModel(context: Application, private val owner: LifecycleOwner) : ViewModel() {

    var delegate: FaultCausesViewModelDelegate? = null

    fun init() {
        delegate?.initialize()
    }
}