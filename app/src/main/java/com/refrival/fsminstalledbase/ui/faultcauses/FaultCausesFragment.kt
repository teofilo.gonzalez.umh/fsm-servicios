package com.refrival.fsminstalledbase.ui.faultcauses

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.utils.CustomViewModelFactory
import kotlinx.android.synthetic.main.fragment_fault_causes.*

class FaultCausesFragment : Base.BaseFragment(), FaultCausesViewModelDelegate {

    private val faultCausesViewModel: FaultCausesViewModel by lazy {
        val factory = CustomViewModelFactory(requireActivity().application, this)
        ViewModelProvider(this, factory).get(FaultCausesViewModel::class.java)
    }

    private val faultCausesAdapter: FaultCausesAdapter by lazy {
        val adapter = FaultCausesAdapter(requireContext())
        adapter
    }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_fault_causes
    }

    override fun init() {
        setHasOptionsMenu(true)
        faultCausesViewModel.delegate = this
        faultCausesViewModel.init()
        rvCauses.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rvCauses.adapter = faultCausesAdapter
    }

    override fun initialize() {
        faultCausesAdapter.setResume(arrayListOf("COMPRESOR", "CUBA PICADA", "FUGA DE CERVEZ", "FUGA DE CO2", "FUGA DE GAS", "GRIFO", "GRUPO DE ARRANQUE", "LIMPIEZA DEL CONDENSADOR"))
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fault_causes_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.close_fault_causes) {
            findNavController().navigate(FaultCausesFragmentDirections.actionFaultCausesFragmentToInstalledBaseFragment())
        }
        return super.onOptionsItemSelected(item)
    }
}