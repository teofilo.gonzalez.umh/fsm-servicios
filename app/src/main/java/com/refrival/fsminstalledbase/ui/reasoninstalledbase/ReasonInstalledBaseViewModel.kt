package com.refrival.fsminstalledbase.ui.reasoninstalledbase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.refrival.fsminstalledbase.repository.db.OperacionDao
import com.refrival.fsminstalledbase.repository.model.AyudaBusqueda
import com.refrival.fsminstalledbase.repository.model.MotivoCierre
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReasonInstalledBaseViewModel
@Inject
    constructor(
        private val sapRepository: SAPRepository,
        private val operacionDao: OperacionDao
    ): ViewModel() {

    private val _etapaLinea = MutableLiveData<List<AyudaBusqueda>>()
    val etapaLinea: LiveData<List<AyudaBusqueda>>
        get() = _etapaLinea

    private val _motivosCierre = MutableLiveData<List<MotivoCierre>>()
    val motivosCierre: LiveData<List<MotivoCierre>>
        get() = _motivosCierre

    fun getAyudaEtapaLinea() {
        viewModelScope.launch {
            _etapaLinea.value = sapRepository.getAyudaEtapaLineaLocal()
        }
    }

    fun getMotivosCierre(ordenId: String, operacionId: String) {
        viewModelScope.launch {
            _motivosCierre.value = sapRepository.getMotivoCierreByOrden(ordenId, operacionId)
        }
    }


    fun setMotivoCierre(ordenId: String, operacionId: String, motivoCierre: MotivoCierre) {
        viewModelScope.launch {
            val operacion = operacionDao.getById(ordenId, operacionId)
            operacion.motivoCierre = motivoCierre
            operacionDao.update(operacion)
        }
    }


}