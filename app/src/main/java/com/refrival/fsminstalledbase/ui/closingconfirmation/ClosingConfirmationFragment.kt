package com.refrival.fsminstalledbase.ui.closingconfirmation

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_closing_confirmation.*

@AndroidEntryPoint
class ClosingConfirmationFragment : Base.BaseFragment(), HeaderFragment.HeaderDelegate {

    private lateinit var ordenId: String
    private lateinit var operacionId: String
    private val closingConfirmationViewModel: ClosingConfirmationViewModel by viewModels()

    private val closingConfirmationAdapter: ClosingConfirmationAdapter by lazy {
        val adapter = ClosingConfirmationAdapter(requireContext())
        adapter
    }

    override fun getXmlLayout(): Int {
        return R.layout.fragment_closing_confirmation
    }

    override fun init() {
        showContentHeader()
        setHasOptionsMenu(true)

        rvClosingConfirmation.adapter = closingConfirmationAdapter
        rvClosingConfirmation.layoutManager = LinearLayoutManager(activity)

        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!

        closingConfirmationViewModel.getClosingConfirmationResult(ordenId, operacionId)
        closingConfirmationViewModel.getComponentsEdited()
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
        closingConfirmationViewModel.operation.observe(viewLifecycleOwner) { operacion ->
            textClosingConfirmationResult.text = operacion.motivoCierre?.descripcionCierre ?: ""
        }
        closingConfirmationViewModel.baseInstalada.observe(viewLifecycleOwner) { baseInstalled ->
            baseInstalled?.let {
                closingConfirmationAdapter.setResume(baseInstalled)
            }
        }
    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.closing_confirmation_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.close_service) {
            findNavController().navigate(ClosingConfirmationFragmentDirections.actionClosingConfirmationFragmentToSignatureFragment())
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goToServicePoint() {
        findNavController().navigate(ClosingConfirmationFragmentDirections.actionClosingConfirmationFragmentToServicePointFragment())
    }
}