package com.refrival.fsminstalledbase.ui.addmaterial

import java.io.Serializable

class Equipment(
    val name: String,
    val id: String,
    val description: String,
    val model: String,
    val tag: String,
    val serialNumber: String,
    val materialId: String,
): Serializable