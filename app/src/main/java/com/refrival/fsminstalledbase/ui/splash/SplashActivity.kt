package com.refrival.fsminstalledbase.ui.splash

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.activity.viewModels
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.User
import com.refrival.fsminstalledbase.ui.login.LoginActivity
import com.refrival.fsminstalledbase.utils.PrefManager
import com.refrival.fsminstalledbase.utils.TAG
import com.refrival.fsminstalledbase.utils.UtilsNetworks
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class SplashActivity : Base.BaseActivity(), SplashViewModelDelegate {

    companion object {
        const val SPLASH_DURATION = 0L
    }

    private val handler = Handler(Looper.getMainLooper())

    private val splashViewModel: SplashViewModel by viewModels()

    private var shouldExecuteOnResume = true


    override fun getXmlLayout(): Int {
        return R.layout.activity_splash
    }

    override fun init() {
        splashViewModel.delegate = this

        val myURI = intent.data
        val operationId = myURI?.getQueryParameter("operationID")
        val orderId = myURI?.getQueryParameter("activity")
        val statusWorkflow = myURI?.getQueryParameter("statusWorkflow") // TODO el status workflow se tiene que hacer algo con el para ir a la firma o al pago o a base instalada
        if (operationId == null || orderId == null) {
            AlertDialog.Builder(this)
                .setTitle(R.string.error_login_title)
                .setMessage(R.string.error_params)
                .setPositiveButton(R.string.accept) { _, _ ->
                    this.finishAffinity()
                }
                .create()
                .show()
            shouldExecuteOnResume = false
            return
        }
        PrefManager(applicationContext).saveOperacionId(operationId)
        PrefManager(applicationContext).saveOrdenId(orderId)

        Log.d(TAG,"Operation: $operationId and statusWorkflow: $statusWorkflow activity: $orderId")
    }

    override fun initListeners() {

    }

    override fun loginSuccess() {
        if (UtilsNetworks.isConnected(this)) {
            // Do login...
            splashViewModel.doLogin(this)
        } else handler.postDelayed({ showMain() }, SPLASH_DURATION)
    }

    override fun loginMissing() {
        handler.postDelayed({ showLogin() }, SPLASH_DURATION)
    }

    override fun onUserSuccess(user: User) {
        splashViewModel.login(user.name.toString(), user.password.toString()).observe(this, {
            if (it != null) handler.postDelayed({ showMain() }, SPLASH_DURATION)
            else handler.postDelayed({ showLogin() }, SPLASH_DURATION)
        })
    }

    private fun showLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun showMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onResume() {
        super.onResume()
        if (shouldExecuteOnResume) {
            splashViewModel.isLoginSuccess(this)
        }
    }

    override fun onPause() {
        handler.removeCallbacksAndMessages(null)
        super.onPause()
    }
}
