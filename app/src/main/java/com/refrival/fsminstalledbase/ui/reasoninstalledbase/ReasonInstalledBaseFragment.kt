package com.refrival.fsminstalledbase.ui.reasoninstalledbase

import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.MotivoCierre
import com.refrival.fsminstalledbase.ui.header.HeaderFragment
import com.refrival.fsminstalledbase.utils.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_reason_installed_base.*

@AndroidEntryPoint
class ReasonInstalledBaseFragment : Base.BaseFragment(), HeaderFragment.HeaderDelegate {

    private val viewModel: ReasonInstalledBaseViewModel by viewModels()

    private lateinit var ordenId: String
    private lateinit var operacionId: String

    override fun getXmlLayout(): Int {
        return R.layout.fragment_reason_installed_base
    }

    override fun init() {
        showContentHeader()
        ordenId = PrefManager(requireContext()).ordenId!!
        operacionId = PrefManager(requireContext()).operacionId!!
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
        var motivosCierre: List<MotivoCierre>
        viewModel.getMotivosCierre(ordenId, operacionId)
        viewModel.motivosCierre.observe(viewLifecycleOwner, { listaMotivosCierre ->
            motivosCierre = listaMotivosCierre

            viewModel.getAyudaEtapaLinea()
            viewModel.etapaLinea.observe(viewLifecycleOwner, { ayudaEtapasLinea ->
                val adapter = ArrayAdapter(
                    requireContext(),
                    R.layout.item_list,
                    ayudaEtapasLinea.map { el -> el.descripcion })
                autoCompleteListType?.setAdapter(adapter)
                autoCompleteListType?.onItemClickListener =
                    AdapterView.OnItemClickListener { parent, view, position, id ->
                        val motivosCierreFiltered = motivosCierre.filter { motivoCierre ->
                            motivoCierre.etapaLinea == ayudaEtapasLinea.get(position).valorId
                        }
                        val adapterReason = ArrayAdapter(
                            requireContext(),
                            R.layout.item_list,
                            motivosCierreFiltered.map { motivoCierre -> motivoCierre.descripcionCierre }
                        )
                        autoCompleteListReason?.setAdapter(adapterReason)
                        autoCompleteListReason?.onItemClickListener =
                            AdapterView.OnItemClickListener {  parent, view, position, id ->
                                viewModel.setMotivoCierre(ordenId, operacionId, motivosCierreFiltered.get(position))
                                findNavController().navigate(
                                    ReasonInstalledBaseFragmentDirections.actionReasonInstalledBaseToInstalledBaseFragment()
                                )
                            }
                    }
            })
        })
    }

    private fun showContentHeader() {
        val headerFragment = HeaderFragment()
        childFragmentManager.beginTransaction()
            .replace(R.id.contentHeader, headerFragment)
            .commitNow()
        headerFragment.delegate = this
    }

    override fun goToServicePoint() {
        findNavController().navigate(ReasonInstalledBaseFragmentDirections.actionReasonInstalledBaseToServicePointFragment())
    }
}