package com.refrival.fsminstalledbase.ui.sync

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SyncViewModel: ViewModel() {
    private val _progress =  MutableLiveData(0)
    val progress: LiveData<Int>
        get() = _progress

    fun setProgress(progress: Int) {
        _progress.value = progress
    }
}
