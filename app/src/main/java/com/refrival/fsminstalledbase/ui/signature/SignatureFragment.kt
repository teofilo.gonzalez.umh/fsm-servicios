package com.refrival.fsminstalledbase.ui.signature

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.service.Sign.*
import com.refrival.fsminstalledbase.ui.confirmpayment.ConfirmPaymentFragmentDirections
import com.refrival.fsminstalledbase.utils.StringUtils
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_signature.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class SignatureFragment : Base.BaseFragment(), CoroutineScope {

    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun getXmlLayout(): Int {
        return R.layout.fragment_signature
    }

    override fun init() {
        val dialog: AlertDialog =
            SpotsDialog.Builder().setContext(context).setMessage(R.string.generating_document)
                .setCancelable(false).build()
        dialog.show()

        val request = getSendDocumentRichRequest()

        launch {
            val response: SendDocumentRichResponse?
            try {
                response = SignApi.retrofitService.sendDocumentRich(request).execute().body()
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.generic_error_call),
                        Toast.LENGTH_LONG
                    ).show()
                }
                dialog.dismiss()

                return@launch
            }

            val responseCode = response?.body?.sendDocumentRichResp?.sendDocumentRichResult?.codigo
            if (responseCode != "0") {
                launch(Dispatchers.Main) {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.error_generating_document_sign),
                        Toast.LENGTH_LONG
                    ).show()
                }
                dialog.dismiss()

                return@launch
            }

            val idDocumento =
                response?.body?.sendDocumentRichResp?.sendDocumentRichResult?.respuestaConcreta?.idDocumento
            var param: String? =
                "empresa=${StringUtils.toBase64("refrival")}&usuario=${StringUtils.toBase64("jclusa")}&password=${
                    StringUtils.toBase64("jclusa")
                }&documento=${StringUtils.toBase64(idDocumento)}&outputCode=${
                    StringUtils.toBase64(
                        "0"
                    )
                }"
            param = StringUtils.toBase64(param)

            try {
                val url = "sign://sign.tresgmg.com?operacion=15&param=${param}"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)

                launch(Dispatchers.Main) {
                    findNavController().navigate(
                        SignatureFragmentDirections.actionSignatureFragmentToSignatureCheckFragment(
                            idDocumento!!
                        )
                    )
                }
            } catch (e:java.lang.Exception) {
                e.printStackTrace()
                launch(Dispatchers.Main) {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(R.string.error_login_title)
                        .setMessage(R.string.error_sphere_sign)
                        .setPositiveButton(R.string.error_sphere_sign_button_intalled) { _, _ ->
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.tresgmg.sign")))
                        }
                        .setNegativeButton(android.R.string.cancel) { _, _ ->
                            (activity as MainActivity).onBackPressed()
                        }
                        .show()
                }
            } finally {
                dialog.dismiss()
            }
        }
    }

    override fun showButtonMaterial() {
        (activity as MainActivity).showButtonMaterial(false)
    }

    override fun initListeners() {
    }

    private fun getSendDocumentRichRequest(): SendDocumentRichRequest {
        val identificadorUsuario = SendDocumentRichRequestIdentificadorUsuario(login = "jclusa")
        val firmaPendienteEntrada = SendDocumentRichRequestFirmaPendienteEntrada(
            idCertificado = "769",
            identificadorUsuario = identificadorUsuario,
            escenario = "FirmaBiometrica"
        )
        val firmasPendientes =
            SendDocumentRichRequestFirmasPendientes(firmaPendienteEntrada = firmaPendienteEntrada)

        val identificadorPlantilla =
            SendDocumentRichRequestIdentificadorPlantilla(id = "2903", nombre = "Danone")

        val codigoBarras = SendDocumentRichRequestValoresDocumentoTipo(
            campoPDF = "CBarras",
            valor = "codigo-barras-prueba-techedge",
            tipoCampoPDF = "TEXT"
        )
        val cif = SendDocumentRichRequestValoresDocumentoTipo(
            campoPDF = "CIF",
            valor = "CIF-prueba-techedge",
            tipoCampoPDF = "TEXT"
        )
        val valoresDocumentoTipo = listOf(codigoBarras, cif)
        val valoresAdicionales =
            SendDocumentRichRequestValoresAdicionales(valoresDocumentoTipo = valoresDocumentoTipo)

        val documento = SendDocumentRichRequestDocumento(
            mailDestino = "jclusa@protonmail.com",
            flujoDocumento = "Aleatorio",
            nombre = "prueba-techedge2.pdf",
            identificadorPlantilla = identificadorPlantilla,
            firmasPendientes = firmasPendientes,
            valoresAdicionales = valoresAdicionales
        )
        val model = SendDocumentRichRequestModel(
            codigoEmpresa = "refrival",
            login = "ws58504",
            password = "rMzFsuvrk0",
            idioma = "ES",
            date = "m23o2MHszn128snAkwkaAKnzLK29s91klalzl19Jjkzj19zlxlnwZNmMnN1812nznNMjsjz9MnznWwqsjzlSAK2znqh0z9134",
            documento = documento
        )
        val body = SendDocumentRichRequestBody(model)
        val request = SendDocumentRichRequest(body)

        return request
    }


}