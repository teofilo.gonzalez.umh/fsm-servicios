package com.refrival.fsminstalledbase.ui.closingconfirmation

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.repository.model.BaseInstalada
import kotlinx.android.synthetic.main.item_failure.view.*

class ClosingConfirmationAdapter(val context: Context) : RecyclerView.Adapter<ClosingConfirmationAdapter.ClosingConfirmationHolder>() {

    private val closingConfirmation = mutableListOf<BaseInstalada>()

    class ClosingConfirmationHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var view: View = v
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClosingConfirmationHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_failure, parent, false)
        return ClosingConfirmationHolder(view)
    }

    override fun onBindViewHolder(holder: ClosingConfirmationHolder, position: Int) {
        closingConfirmation.let { item ->
            holder.view.imageFailure.setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.ic_baseline))
            holder.view.textMaterial.text = item[position].textoComponente
            holder.view.textCause.text = "Causa Avería $position"
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setResume(resume: List<BaseInstalada>) {
        this.closingConfirmation.clear()
        this.closingConfirmation.addAll(resume)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return closingConfirmation.size
    }
}
