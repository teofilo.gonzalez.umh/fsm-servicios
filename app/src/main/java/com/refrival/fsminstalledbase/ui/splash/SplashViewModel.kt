package com.refrival.fsminstalledbase.ui.splash

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import com.refrival.fsminstalledbase.repository.model.User
import com.refrival.fsminstalledbase.repository.service.fsm.FSMRepository
import com.refrival.fsminstalledbase.repository.service.fsm.FSMService
import com.refrival.fsminstalledbase.repository.service.fsm.FSMServiceImpl
import com.refrival.fsminstalledbase.utils.Crypt
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class SplashViewModel
    @Inject
    constructor(
        refrivalDatabase: RefrivalDatabase
    ) : ViewModel() {

    var delegate: SplashViewModelDelegate? = null
    private var refrivalRepository = refrivalDatabase.userDao()

    private var fsmRepository: FSMRepository = FSMServiceImpl()

    fun isLoginSuccess(owner: LifecycleOwner) {
        refrivalRepository.get().observe(owner) { user ->
            if (user != null) delegate?.loginSuccess()
            else delegate?.loginMissing()
        }
    }

    fun login(userName: String, userPassword: String) : MutableLiveData<User?> {
        val liveData : MutableLiveData<User?> = MutableLiveData<User?>()
        fsmRepository.login(Crypt.decodeAndDecrypt(userName), Crypt.decodeAndDecrypt(userPassword), object : FSMService.CallbackResponse<User>{
            override fun onResponse(response: User) {
                liveData.value = response
            }

            override fun onFailure(t: Throwable, res: Response<*>?) {
                liveData.value = null
            }
        })
        return liveData
    }

    fun doLogin(owner: LifecycleOwner) {
        refrivalRepository.get().observe(owner) { user ->
            if (user != null) delegate?.onUserSuccess(user)
            else delegate?.loginMissing()
        }
    }
}