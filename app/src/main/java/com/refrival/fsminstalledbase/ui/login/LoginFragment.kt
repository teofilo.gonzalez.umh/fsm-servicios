package com.refrival.fsminstalledbase.ui.login

import android.app.AlertDialog
import android.view.View
import androidx.fragment.app.viewModels
import com.refrival.fsminstalledbase.MainActivity
import com.refrival.fsminstalledbase.R
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.repository.model.User
import com.refrival.fsminstalledbase.repository.model.UserRequest
import com.refrival.fsminstalledbase.utils.PrefManager
import com.refrival.fsminstalledbase.utils.SAP_USER_TECNICO_1
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_login.*

@AndroidEntryPoint
class LoginFragment : Base.BaseFragment(), LoginViewModelDelegate {

    private val loginViewModel: LoginViewModel by viewModels()

    override fun getXmlLayout(): Int {
        return R.layout.fragment_login
    }

    override fun init() {
        loginViewModel.delegate = this
        loginViewModel.init()
    }

    override fun initialize() {

    }

    override fun showButtonMaterial() {

    }

    override fun initListeners() {
        btnLogin.setOnClickListener {
            showProgress(true)
            loginViewModel.login(
                editTextUserName.text.toString(),
                editTextPassword.text.toString()
            )
                .observe(viewLifecycleOwner, { user ->
                    if (user != null) {
                        // Send user and firebase token to server
                        sendUserAndTokenFirebase(user)
                        showProgress(false)
                        // Show Main
                        (activity as LoginActivity).goToMain()
                    } else {
                        // Show error
                        showProgress(false)
                        showDialogErrorLogin()
                    }
                })
        }
    }

    private fun showDialogErrorLogin() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error_login_title)
            .setMessage(R.string.error_login_description)
            .setPositiveButton(R.string.accept) { view, _ ->
                view.dismiss()
            }
            .create()
            .show()
    }

    private fun showProgress(showProgress: Boolean) {
        if (showProgress) {
            progress.visibility = View.VISIBLE
            inputLayoutUserName.isEnabled = false
            editTextUserName.isEnabled = false
            inputLayoutPassword.isEnabled = false
            editTextPassword.isEnabled = false
            btnLogin.isEnabled = false
        } else {
            progress.visibility = View.INVISIBLE
            inputLayoutUserName.isEnabled = true
            editTextUserName.isEnabled = true
            inputLayoutPassword.isEnabled = true
            editTextPassword.isEnabled = true
            btnLogin.isEnabled = true
        }
    }

    private fun sendUserAndTokenFirebase(user: User) {
        val userRequest = UserRequest()
        userRequest.TecnicoId = SAP_USER_TECNICO_1
        userRequest.DeviceId = PrefManager(requireContext()).getFirebaseToken
        loginViewModel.sentUserAndToken(userRequest)
    }
}