package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "orden_table")
data class Orden(
    @Json(name = "OrdenId")
    @PrimaryKey
    var ordenId: String,

    @Json(name = "OperacionId")
    var operacionId: String,

    @Json(name = "OrdenDesc")
    var ordenDesc: String,

    @Json(name = "Observacion")
    var observacion: String,

    @Json(name = "FueraHorario")
    var fueraHorario: String,

    @Json(name = "ServicioEmergencia")
    var servicioEmergencia: String,
) {
    @Json(name = "Operaciones")
    @Ignore
    var operaciones: Operaciones? = null
}

data class Operaciones(
    @Json(name = "results")
    var results: List<Operacion?>? = null
)