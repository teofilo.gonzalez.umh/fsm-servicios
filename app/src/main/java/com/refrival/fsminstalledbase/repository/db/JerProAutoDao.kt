package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.JerProAuto

@Dao
abstract class JerProAutoDao {
    @Insert
    abstract fun insert(jerProAuto: JerProAuto)

    @Delete
    abstract fun delete(jerProAuto: JerProAuto)

    @Update
    abstract fun update(jerProAuto: JerProAuto)

    @Query("DELETE FROM jer_pro_auto_table")
    abstract fun deleteAll()
}
