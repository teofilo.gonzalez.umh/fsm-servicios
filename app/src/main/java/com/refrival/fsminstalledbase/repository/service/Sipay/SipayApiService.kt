package com.refrival.fsminstalledbase.repository.service.Sipay

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

private const val BASE_URL = "https://sandbox.sipay.es/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface SipayApiService {
    @POST("mdwr/v1/authentication")
    fun authenticate(@Header("Content-Signature") signature: String, @Body body: RequestBody): Call<AuthenticationResponse>

    @POST("mdwr/v1/authentication/confirm")
    fun confirmAuthentication(@Header("Content-Signature") signature: String, @Body body: RequestBody): Call<ConfirmAuthenticationResponse>
}

object SipayApi {
    val retrofitService : SipayApiService by lazy { retrofit.create(SipayApiService::class.java) }
}