package com.refrival.fsminstalledbase.repository.service.fsm

import com.refrival.fsminstalledbase.utils.BASE_URL
import com.refrival.fsminstalledbase.utils.CLIENT_ID
import com.refrival.fsminstalledbase.utils.CLIENT_SECRET
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class FSMService {

    interface CallbackResponse<T> {
        fun onResponse(response: T)
        fun onFailure(t: Throwable, res: Response<*>? = null)
    }

    val fsmApi: FSMApi
    private val baseURL = BASE_URL

    init {

        val timeout = 12000L

        val client = OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .addInterceptor(BasicAuthInterceptor(CLIENT_ID, CLIENT_SECRET))
            .build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseURL)
            .build()

        fsmApi = retrofit.create(FSMApi::class.java)
    }
}

class BasicAuthInterceptor(clientId: String, clientSecret: String): Interceptor {
    private var credentials: String = Credentials.basic(clientId, clientSecret)

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder()
            .header("Authorization", credentials)
            .header("Content-Type","application/x-www-form-urlencoded")
            .header("Accept","application/json")
            .build()
        return chain.proceed(request)
    }
}