package com.refrival.fsminstalledbase.repository.model

import com.google.gson.annotations.SerializedName

data class UserRequest(

	@field:SerializedName("TecnicoId")
	var TecnicoId: String? = null,

	@field:SerializedName("DeviceId")
	var DeviceId: String? = null
)
