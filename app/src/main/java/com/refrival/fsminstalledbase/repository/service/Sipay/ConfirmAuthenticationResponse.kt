package com.refrival.fsminstalledbase.repository.service.Sipay

import com.squareup.moshi.Json

data class ConfirmAuthenticationResponse(
    val type: String,
    val code: String,
    val detail: String,
    val description: String,
    val uuid: String,
)
