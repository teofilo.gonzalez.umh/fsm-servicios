package com.refrival.fsminstalledbase.repository.service.sap

import com.refrival.fsminstalledbase.repository.model.EtapaLineaResponse
import com.refrival.fsminstalledbase.repository.model.Odata
import com.refrival.fsminstalledbase.repository.model.UserRequest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface SAPApi {

    @GET("TecnicoSet(TecnicoId='{TecnicoId}',DeviceId='{DeviceId}')?\$expand=Ordenes,Ordenes/Operaciones/Jerarquias/MotivosCierre,Ordenes/Operaciones/Bi,Ordenes/Operaciones/Detalles,Ordenes/Operaciones/JerProAuto,Ordenes/Operaciones/JerProNivel&\$format=json&sap-client=200")
    suspend fun getOdata(
        @Path("TecnicoId") tecnicoId: String,
        @Path("DeviceId") deviceId: String
    ): Response<Odata>

    @GET("ValoresEtapaLineaSet?\$format=json&sap-client=200")
    suspend fun getAyudaEtapaLinea(): Response<EtapaLineaResponse>

    @Headers("X-Requested-With: x")
    @POST("TecnicoSet?sap-client=200")
    fun sendUserAndToken(@Body resultsRequest: UserRequest): Call<ResponseBody>

    @GET("TecnicoSet(TecnicoId='{TecnicoId}',DeviceId='{DeviceId}')?\$expand=Stock&\$format=json&sap-client=200")
    suspend fun getStock(
        @Path("TecnicoId") tecnicoId: String,
        @Path("DeviceId") deviceId: String
    ): Response<Odata>

    @GET("TecnicoSet(TecnicoId='{TecnicoId}',DeviceId='{DeviceId}')?\$expand=Materiales/MaterialesProyecto&\$format=json&sap-client=200")
    suspend fun getMateriales(
        @Path("TecnicoId") tecnicoId: String,
        @Path("DeviceId") deviceId: String
    ): Response<Odata>
}