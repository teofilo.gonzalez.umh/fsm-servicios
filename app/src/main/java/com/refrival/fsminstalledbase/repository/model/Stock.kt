package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "stock_table", primaryKeys= [ "materialId", "centroId", "almacenId", "numeroSerieId", "numeroSerieFabId", "numeroPiezaId"] )
data class Stock(
    @Json(name = "MaterialId")
    var materialId: String,

    @Json(name = "CentroId")
    var centroId: String,

    @Json(name = "AlmacenId")
    var almacenId: String,

    @Json(name = "NumeroSerieId")
    var numeroSerieId: String,

    @Json(name = "NumeroSerieFabId")
    var numeroSerieFabId: String,

    @Json(name = "NumeroPiezaId")
    var numeroPiezaId: String,

    @Json(name = "Lote")
    var lote: String,

    @Json(name = "TextoMaterial")
    var textoMaterial: String,

    @Json(name = "StatusSistema")
    var statusSistema: String,

    @Json(name = "Cantidad")
    var cantidad: Int,

    @Json(name = "DenominacionTipo")
    var denominacionTipo: String
)