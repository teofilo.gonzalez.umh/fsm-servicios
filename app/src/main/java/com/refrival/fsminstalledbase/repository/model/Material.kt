package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "material_table")
data class Material(
    @Json(name="MaterialId")
    @PrimaryKey
    var materialId: String,

    @Json(name="Canal")
    var canal: String,

    @Json(name="Clase")
    var clase: String,

    @Json(name="FormatoTag")
    var formatoTag: String,

    @Json(name="NodosMaximos")
    var nodosMaximos: Int,

    @Json(name="NodosMinimos")
    var nodosMinimos: Int,

    @Json(name="Tag")
    var tag: String,

    @Json(name="JerarquiaId")
    var jerarquiaId: String,
) {
    @Json(name="MaterialesProyecto")
    @Ignore
    var materialesProyecto: MaterialesProyectoOdata? = null
}

data class MaterialesProyectoOdata(
    @Json(name="results")
    val results: List<MaterialesProyecto?>? = null
)