package com.refrival.fsminstalledbase.repository.service.fsm

import com.refrival.fsminstalledbase.repository.model.User
import com.refrival.fsminstalledbase.utils.GRANT_TYPE
import com.refrival.fsminstalledbase.utils.RESPONSE_TYPE
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FSMServiceImpl : FSMRepository {

    override fun login(userName: String, userPassword: String, cb: FSMService.CallbackResponse<User>) {
        FSMService().fsmApi.login(GRANT_TYPE, RESPONSE_TYPE, userName, userPassword)
            .enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if (response.isSuccessful && response.body() != null) {
                        cb.onResponse(response.body()!!)
                    } else {
                        cb.onFailure(Throwable(response.message()), response)
                    }
                }
                override fun onFailure(call: Call<User>, t: Throwable) {
                    cb.onFailure(t)
                }
            })
    }
}