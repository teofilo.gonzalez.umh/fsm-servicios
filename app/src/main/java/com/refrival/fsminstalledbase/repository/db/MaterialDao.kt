package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.Material

@Dao
abstract class MaterialDao {
    @Insert
    abstract fun insert(material: Material)

    @Delete
    abstract fun delete(material: Material)

    @Update
    abstract fun update(material: Material)

    @Query("DELETE FROM material_table")
    abstract fun deleteAll()
}
