package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.Ignore
import com.squareup.moshi.Json

@Entity(
    tableName = "jerarquia_table",
    primaryKeys = ["ordenId", "operacionId", "jerarquiaId", "tipoOrdenId", "proyectoId", "tipoActividadId", "claveModId", "fueraHorarioId", "prioridadId"]
)
data class Jerarquia(
    @Json(name = "OrdenId")
    var ordenId: String,

    @Json(name = "OperacionId")
    var operacionId: String,

    @Json(name = "JerarquiaId")
    var jerarquiaId: String,

    @Json(name = "TipoOrdenId")
    var tipoOrdenId: String,

    @Json(name = "ProyectoId")
    var proyectoId: String,

    @Json(name = "TipoActividadId")
    var tipoActividadId: String,

    @Json(name = "ClaveModId")
    var claveModId: String,

    @Json(name = "FueraHorarioId")
    var fueraHorarioId: String,

    @Json(name = "PrioridadId")
    var prioridadId: String,

    @Json(name = "MaterialId")
    var materialId: String,

    @Json(name = "Estado")
    var estado: String,

    @Json(name = "FechaInicioValidez")
    var fechaInicioValidez: String,

    @Json(name = "FechaFinValidez")
    var fechaFinValidez: String,
) {
    @Json(name = "MotivosCierre")
    @Ignore
    var motivosCierre: MotivosCierreOdata? = null
}

data class MotivosCierreOdata(
    @Json(name = "results")
    var results: List<MotivoCierre?>? = null
)