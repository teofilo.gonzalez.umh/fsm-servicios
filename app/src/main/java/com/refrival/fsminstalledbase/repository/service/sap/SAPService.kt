package com.refrival.fsminstalledbase.repository.service.sap

import com.refrival.fsminstalledbase.utils.SAP_BASE_URL
import com.refrival.fsminstalledbase.utils.SAP_PASSWORD
import com.refrival.fsminstalledbase.utils.SAP_USER
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class SAPService {

    interface CallbackResponse<T> {
        fun onResponse(response: T)
        fun onFailure(t: Throwable, res: Response<*>? = null)
    }

    val sapApi: SAPApi
    private val baseURL = SAP_BASE_URL

    init {

        val timeout = 12000L

        val client = OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .addInterceptor(BasicAuthInterceptor(SAP_USER, SAP_PASSWORD))
            .build()

        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(baseURL)
            .build()

        sapApi = retrofit.create(SAPApi::class.java)
    }
}

class BasicAuthInterceptor(sapUser: String, sapPassword: String): Interceptor {
    private var credentials: String = Credentials.basic(sapUser, sapPassword)

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder()
            .header("Authorization", credentials)
            .header("Accept","application/json")
            .build()
        return chain.proceed(request)
    }
}
