package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "motivo_cierre_table",
    primaryKeys = ["ordenId", "operacionId", "jerarquiaId", "claveModId", "tipoActividadId", "motivoCierre", "etapaLinea"]
)
data class MotivoCierre(
    var ordenId: String = "",

    var operacionId: String = "",

    @Json(name="JerarquiaId")
    var jerarquiaId: String,

    @Json(name="ClaveModId")
    var claveModId: String,

    @Json(name="TipoActividadId")
    var tipoActividadId: String,

    @Json(name="MotivoCierre")
    var motivoCierre: String,

    @Json(name="EtapaLinea")
    var etapaLinea: String,

    @Json(name="DescripcionCierre")
    var descripcionCierre: String,

    @Json(name="Proyecto")
    var proyecto: String,

    @Json(name="Cliente")
    var cliente: String,

    @Json(name="TipoOrden")
    var tipoOrden: String,

    @Json(name="Material")
    var material: String,

    @Json(name="ChkUrgente")
    var chkUrgente: String,

    @Json(name="ChkFuerahorario")
    var chkFuerahorario: String,

    @Json(name="Facturable")
    var facturable: String,

    @Json(name="Activallbi")
    var activallbi: Boolean,

    @Json(name="Inactallbi")
    var inactallbi: Boolean,

    @Json(name="Acticomp")
    var acticomp: Boolean,

    @Json(name="Inactcomp")
    var inactcomp: Boolean,

    @Json(name="Inacjerar")
    var inacjerar: Boolean,

    @Json(name="Actijerar")
    var actijerar: Boolean,

    @Json(name="Activabi")
    var activabi: Boolean,

    @Json(name="Ajustbi")
    var ajustbi: String,

    @Json(name="Nuevaord")
    var nuevaord: Boolean,

    @Json(name="Activiadic")
    var activiadic: Boolean,

    @Json(name="Nuevactivadic")
    var nuevactivadic: String,

    @Json(name="Anulprev")
    var anulprev: Boolean,

    @Json(name="Creaprev")
    var creaprev: Boolean,

    @Json(name="Continuprev")
    var continuprev: Boolean,

    @Json(name="Partetrab")
    var partetrab: Boolean,

    @Json(name="Motivaver")
    var motivaver: Boolean,

    @Json(name="Reqcierre")
    var reqcierre: String
)
