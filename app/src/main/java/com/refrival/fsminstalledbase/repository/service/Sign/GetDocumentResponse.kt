package com.refrival.fsminstalledbase.repository.service.Sign

import org.simpleframework.xml.*


@Root(name = "soap:Envelope")
@NamespaceList(
    Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
    Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
    Namespace(reference = "http://www.w3.org/2003/05/soap-envelope", prefix = "soap")
)
class GetDocumentResponse {
    @field:Element(name = "Body", required = false)
    var body: GetDocumentResponseBody? = null
}

@Root(name = "soap:Body", strict = false)
class GetDocumentResponseBody {
    @field:Element(name = "getDocumentResponse", required = false)
    var getDocumentResp: GetDocumentResp? = null
}

@Root(name = "getDocumentResponse", strict = false)
@Namespace(reference = "http://mobilesign.com/")
class GetDocumentResp {
    @field:Element(name = "getDocumentResult", required = false)
    var getDocumentResult: GetDocumentResponseResult? = null
}

@Root(name = "getDocumentResult", strict = false)
class GetDocumentResponseResult {
    @field:Element(name = "resultado", required = false)
    var resultado: GetDocumentResponseResultado? = null

    @field:Element(name = "fichero", required = false)
    var fichero: GetDocumentResponseFichero? = null
}

@Root(name = "resultado", strict = false)
class GetDocumentResponseResultado {
    @field:Element(name = "codigoError", required = false)
    var codigoError: String? = null
}

@Root(name = "fichero", strict = false)
class GetDocumentResponseFichero {
    @field:Element(name = "IdEstado", required = false)
    var idEstado: String? = null
}
