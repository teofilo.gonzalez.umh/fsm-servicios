package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.JerProNivel

@Dao
abstract class JerProNivelDao {
    @Insert
    abstract fun insert(jerProNivel: JerProNivel)

    @Delete
    abstract fun delete(jerProNivel: JerProNivel)

    @Update
    abstract fun update(jerProNivel: JerProNivel)

    @Query("DELETE FROM jer_pro_nivel_table")
    abstract fun deleteAll()
}
