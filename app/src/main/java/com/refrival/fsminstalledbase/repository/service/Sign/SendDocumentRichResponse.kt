package com.refrival.fsminstalledbase.repository.service.Sign

import org.simpleframework.xml.*


@Root(name = "soap:Envelope")
@NamespaceList(
    Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
    Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
    Namespace(reference = "http://www.w3.org/2003/05/soap-envelope", prefix = "soap")
)
class SendDocumentRichResponse {
    @field:Element(name = "Body", required = false)
    var body: SendDocumentRichResponseBody? = null
}

@Root(name = "soap:Body", strict = false)
class SendDocumentRichResponseBody {
    @field:Element(name = "sendDocumentRichResponse", required = false)
    var sendDocumentRichResp: SendDocumentRichResp? = null
}

@Root(name = "sendDocumentRichResponse", strict = false)
@Namespace(reference = "http://mobilesign.com/")
class SendDocumentRichResp {
    @field:Element(name = "sendDocumentRichResult", required = false)
    var sendDocumentRichResult: SendDocumentRichResult? = null
}

@Root(name = "sendDocumentRichResult", strict = false)
class SendDocumentRichResult {
    @field:Element(name = "Codigo", required = false)
    var codigo: String? = null

    @field:Element(name = "Mensaje", required = false)
    var mensaje: String? = null

    @field:Element(name = "RespuestaConcreta", required = false)
    var respuestaConcreta: SendDocumentRichRespuestaConcreta? = null
}

@Root(name = "RespuestaConcreta", strict = false)
@Namespace(reference = "http://3gmobilesign.com/", prefix = "q1")
class SendDocumentRichRespuestaConcreta {
    @field:Element(name = "IdDocumento", required = false)
    var idDocumento: String? = null
}

