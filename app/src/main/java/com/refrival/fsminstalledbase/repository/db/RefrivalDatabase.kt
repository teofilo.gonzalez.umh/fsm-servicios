package com.refrival.fsminstalledbase.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.refrival.fsminstalledbase.repository.model.*

@Database(entities =
[
    Orden::class,
    Operacion::class,
    Jerarquia::class,
    DetalleOperacion::class,
    JerProAuto::class,
    JerProNivel::class,
    User::class,
    BaseInstalada::class,
    Stock::class,
    MotivoCierre::class,
    AyudaBusqueda::class,
    Material::class,
    MaterialesProyecto::class,
], version = 1, exportSchema = false)

@TypeConverters(Converters::class)
abstract class RefrivalDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun ordenDao(): OrdenDao
    abstract fun operacionDao(): OperacionDao
    abstract fun jerarquiaDao(): JerarquiaDao
    abstract fun detalleOperacionDao(): DetalleOperacionDao
    abstract fun jerProAutoDao(): JerProAutoDao
    abstract fun jerProNivelDao(): JerProNivelDao
    abstract fun baseInstalledDao(): BaseInstaladaDao
    abstract fun motivoCierreDao(): MotivoCierreDao
    abstract fun ayudaBusquedaDao(): AyudaBusquedaDao
    abstract fun stockDao(): StockDao
    abstract fun materialDao(): MaterialDao
    abstract fun materialesProyectoDao(): MaterialesProyectoDao

    companion object {

        private var INSTANCE: RefrivalDatabase? = null

        fun getInstance(context: Context): RefrivalDatabase {
            return INSTANCE ?: synchronized(RefrivalDatabase::class) {
                INSTANCE ?: Room.databaseBuilder(
                    context.applicationContext,
                    RefrivalDatabase::class.java,
                    "refrival.db"
                )
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build().also { INSTANCE = it }
            }
        }
    }
}