package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.AyudaBusqueda
import com.refrival.fsminstalledbase.utils.ETAPA_LINEA_AYUDA

@Dao
abstract class AyudaBusquedaDao {
    @Query("SELECT * from ayuda_busqueda_table WHERE campoAyuda == '$ETAPA_LINEA_AYUDA'")
    abstract suspend fun getEtapaLinea() : List<AyudaBusqueda>

    @Insert
    abstract fun insert(ayudaBusqueda: AyudaBusqueda)

    @Delete
    abstract fun delete(ayudaBusqueda: AyudaBusqueda)

    @Update
    abstract fun update(ayudaBusqueda: AyudaBusqueda)

    @Query("DELETE FROM ayuda_busqueda_table")
    abstract fun deleteAll()
}