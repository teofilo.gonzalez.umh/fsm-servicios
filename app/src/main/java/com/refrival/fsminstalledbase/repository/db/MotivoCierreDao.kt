package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.MotivoCierre

@Dao
abstract class MotivoCierreDao {
    @Query("SELECT * from motivo_cierre_table WHERE ordenId == :ordenId AND operacionId == :operacionId  GROUP BY motivoCierre, tipoActividadId, claveModId, etapaLinea")
    abstract suspend fun getMotivoCierreByOrden(ordenId: String, operacionId: String) : List<MotivoCierre>

    @Insert
    abstract fun insert(motivoCierre: MotivoCierre)

    @Delete
    abstract fun delete(motivoCierre: MotivoCierre)

    @Update
    abstract fun update(motivoCierre: MotivoCierre)

    @Query("DELETE FROM motivo_cierre_table")
    abstract fun deleteAll()
}