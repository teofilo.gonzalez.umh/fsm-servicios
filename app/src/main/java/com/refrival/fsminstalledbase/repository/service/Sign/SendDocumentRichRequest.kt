package com.refrival.fsminstalledbase.repository.service.Sign

import org.simpleframework.xml.*


@Root(name = "soap12:Envelope")
@NamespaceList(
    Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
    Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
    Namespace(reference = "http://www.w3.org/2003/05/soap-envelope", prefix = "soap12"),
    Namespace(reference = "http://3gmobilesign.com/", prefix = "gmob")
)
data class SendDocumentRichRequest(
    @field:Element(name = "soap12:Body", required = false)
    val body: SendDocumentRichRequestBody
)

@Root(name = "soap12:Body", strict = false)
data class SendDocumentRichRequestBody(
    @field:Element(name = "sendDocumentRich", required = false)
    val sendDocumentRich: SendDocumentRichRequestModel
)

@Namespace(reference = "http://mobilesign.com/")
data class SendDocumentRichRequestModel(
    @field:Element(name = "codigoEmpresa", required = false)
    val codigoEmpresa: String,

    @field:Element(name = "login", required = false)
    val login: String,

    @field:Element(name = "password", required = false)
    val password: String,

    @field:Element(name = "idioma", required = false)
    val idioma: String,

    @field:Element(name = "date", required = false)
    val date: String,

    @field:Element(name = "documento", required = false)
    val documento: SendDocumentRichRequestDocumento
)

data class SendDocumentRichRequestDocumento(
    @field:Element(name = "MailDestino", required = false)
    val mailDestino: String,

    @field:Element(name = "FlujoDocumento", required = false)
    val flujoDocumento: String,

    @field:Element(name = "Nombre", required = false)
    val nombre: String,

    @field:Element(name = "IdentificadorPlantilla", required = false)
    val identificadorPlantilla: SendDocumentRichRequestIdentificadorPlantilla,

    @field:Element(name = "FirmasPendientes", required = false)
    val firmasPendientes: SendDocumentRichRequestFirmasPendientes,

    @field:Element(name = "ValoresAdicionales", required = false)
    val valoresAdicionales: SendDocumentRichRequestValoresAdicionales
)

data class SendDocumentRichRequestIdentificadorPlantilla(
    @field:Element(name = "Id", required = false)
    val id: String,

    @field:Element(name = "Nombre", required = false)
    val nombre: String
)

data class SendDocumentRichRequestFirmasPendientes(
    @field:Element(name = "FirmaPendienteEntrada", required = false)
    val firmaPendienteEntrada: SendDocumentRichRequestFirmaPendienteEntrada
)

data class SendDocumentRichRequestFirmaPendienteEntrada(
    @field:Element(name = "IdCertificado", required = false)
    val idCertificado: String,

    @field:Element(name = "Escenario", required = false)
    val escenario: String,

    @field:Element(name = "IdentificadorUsuario", required = false)
    val identificadorUsuario: SendDocumentRichRequestIdentificadorUsuario
)

data class SendDocumentRichRequestIdentificadorUsuario(
    @field:Element(name = "gmob:Login", required = false)
    val login: String
)

data class SendDocumentRichRequestValoresAdicionales(
    @field:ElementList(entry = "ValoresDocumentoTipo", inline = true)
    val valoresDocumentoTipo: List<SendDocumentRichRequestValoresDocumentoTipo>
)

data class SendDocumentRichRequestValoresDocumentoTipo(
    @field:Element(name = "CampoPDF", required = false)
    val campoPDF: String,

    @field:Element(name = "Valor", required = false)
    val valor: String,

    @field:Element(name = "TipoCampoPDF", required = false)
    val tipoCampoPDF: String
)


