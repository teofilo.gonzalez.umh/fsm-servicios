package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.Ignore
import com.refrival.fsminstalledbase.repository.model.enums.OperacionStatus
import com.squareup.moshi.Json

@Entity(tableName = "operacion_table", primaryKeys= [ "ordenId", "operacionId" ] )
data class Operacion(
    @Json(name = "OrdenId")
    var ordenId: String,

    @Json(name = "OperacionId")
    var operacionId: String,

    var status: OperacionStatus = OperacionStatus.TODO,

    var motivoCierre: MotivoCierre? = null,

    @Json(name = "TipoActividadId")
    var tipoActividadId: String,

    @Json(name = "CentroId")
    var centroId: String,

    @Json(name = "ClaveModId")
    var claveModId: String,

    @Json(name = "ClaveModDesc")
    var claveModDesc: String,

    @Json(name = "Observaciones")
    var observaciones: String,

    @Json(name = "UbicacionId")
    var ubicacionId: String,

    @Json(name = "EquipoId")
    var equipoId: String,

    @Json(name = "ProyectoId")
    var proyectoId: String,

    @Json(name = "EstadoId")
    var estadoId: String,

    ) {
    @Json(name = "Jerarquias")
    @Ignore
    var jerarquias: Jerarquias? = null

    @Json(name = "Detalles")
    @Ignore
    var detallesOperacion: DetallesOperacion? = null

    @Json(name = "Bi")
    @Ignore
    var bi: Bi? = null

    @Json(name = "JerProAuto")
    @Ignore
    var jerProAuto: JerProAutoOdata? = null

    @Json(name = "JerProNivel")
    @Ignore
    var jerProNivel: JerProNivelOdata? = null
}

data class Jerarquias(
    @Json(name = "results")
    var results: List<Jerarquia?>? = null
)

data class DetallesOperacion(
    @Json(name = "results")
    var results: List<DetalleOperacion?>? = null
)

data class Bi(
    @Json(name = "results")
    var results: List<BaseInstalada?>? = null
)

data class JerProAutoOdata(
    @Json(name = "results")
    var results: List<JerProAuto?>? = null
)

data class JerProNivelOdata(
    @Json(name = "results")
    var results: List<JerProNivel?>? = null
)