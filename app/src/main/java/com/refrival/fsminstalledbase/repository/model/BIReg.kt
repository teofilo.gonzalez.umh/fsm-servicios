package com.refrival.fsminstalledbase.repository.model

import androidx.room.Embedded

class BIReg {
    @Embedded
    var baseInstalada: BaseInstalada? = null

    var level: Int = -1
}
