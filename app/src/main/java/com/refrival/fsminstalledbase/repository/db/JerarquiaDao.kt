package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.Jerarquia

@Dao
abstract class JerarquiaDao {
    @Insert
    abstract fun insert(jerarquia: Jerarquia)

    @Delete
    abstract fun delete(jerarquia: Jerarquia)

    @Update
    abstract fun update(jerarquia: Jerarquia)

    @Query("DELETE FROM jerarquia_table")
    abstract fun deleteAll()
}
