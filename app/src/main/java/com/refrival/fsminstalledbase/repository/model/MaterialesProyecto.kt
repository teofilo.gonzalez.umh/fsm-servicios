package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import com.squareup.moshi.Json


@Entity(tableName = "materiales_proyecto_table", primaryKeys= [ "materialId", "proyectoId" ] )
data class MaterialesProyecto(
    @Json(name="MaterialId")
    var materialId: String,

    @Json(name="ProyectoId")
    var proyectoId: String
)