package com.refrival.fsminstalledbase.repository.service.fsm

import com.refrival.fsminstalledbase.repository.model.User


interface FSMRepository {
    fun login(userName: String, userPassword: String, cb: FSMService.CallbackResponse<User>)
}