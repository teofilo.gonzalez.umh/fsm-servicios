package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = User.TABLE_NAME)
data class User(

	@PrimaryKey
	@field:SerializedName("user_id")
	var userId: Int? = null,

	@field:SerializedName("tenant_id")
	var tenantId: String? = null,

	@field:SerializedName("user_email")
	var userEmail: String? = null,

	@field:SerializedName("token_type")
	var tokenType: String? = null,

	@field:SerializedName("access_token")
	var accessToken: String? = null,

	@field:SerializedName("cluster_url")
	var clusterUrl: String? = null,

	@field:SerializedName("account_id")
	var accountId: Int? = null,

	@field:SerializedName("scope")
	var scope: String? = null,

	@field:SerializedName("expires_in")
	var expiresIn: Int? = null,

	@field:SerializedName("user")
	var user: String? = null,

	@field:SerializedName("account")
	var account: String? = null,

	@field:SerializedName("password")
	var password: String? = null,

	@field:SerializedName("name")
	var name: String? = null

) : Serializable {
	companion object {
		const val TABLE_NAME = "user_table"
	}
}
