package com.refrival.fsminstalledbase.repository.service.sap

import com.refrival.fsminstalledbase.repository.db.*
import com.refrival.fsminstalledbase.repository.model.*
import com.refrival.fsminstalledbase.utils.DEVICE_ID
import com.refrival.fsminstalledbase.utils.ETAPA_LINEA_AYUDA
import com.refrival.fsminstalledbase.utils.TECNICO_ID
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class SAPRepository
@Inject
constructor(
    private val ordenDao: OrdenDao,
    private val operacionDao: OperacionDao,
    private val jerarquiaDao: JerarquiaDao,
    private val detalleOperacionDao: DetalleOperacionDao,
    private val jerProAutoDao: JerProAutoDao,
    private val jerProNivelDao: JerProNivelDao,
    private val baseInstaladaDao: BaseInstaladaDao,
    private val motivoCierreDao: MotivoCierreDao,
    private val ayudaBusquedaDao: AyudaBusquedaDao,
    private val stockDao: StockDao,
    private val materialDao: MaterialDao,
    private val materialesProyectoDao: MaterialesProyectoDao
) {
    fun getOdata() = flow {
        jerarquiaDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta
        jerProAutoDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta
        jerProNivelDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta
        motivoCierreDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta

        ordenDao.deleteAll()
        detalleOperacionDao.deleteAll() // TODO es correcto?
        operacionDao.getAllTODO().forEach { operacion ->
            baseInstaladaDao.deleteAllByOrdenOperacion(operacion.ordenId, operacion.operacionId)
            operacionDao.delete(operacion)
        }
        val operacionesPendientes = operacionDao.getAllNotTODO()
            .map { operacion -> "${operacion.ordenId}|${operacion.operacionId}" }

        val response = SAPService().sapApi.getOdata(TECNICO_ID, DEVICE_ID)
        val odata = response.body()

        if (!response.isSuccessful) {
            error("Failed to get oData")
        }

        if (odata != null) {
            odata.D?.ordenes?.results?.forEach { orden ->
                ordenDao.insert(orden!!)
                orden?.operaciones?.results?.forEach { operacion ->
                    if (!operacionesPendientes.contains("${operacion?.ordenId}|${operacion?.operacionId}")) {
                        operacionDao.insert(operacion!!)
                    }
                    operacion?.jerarquias?.results?.forEach { jerarquia ->
                        jerarquia?.ordenId = orden.ordenId
                        jerarquia?.operacionId = operacion.operacionId
                        jerarquiaDao.insert(jerarquia!!)
                        jerarquia?.motivosCierre?.results?.forEach { motivoCierre ->
                            motivoCierre?.ordenId = orden.ordenId
                            motivoCierre?.operacionId = operacion.operacionId
                            motivoCierreDao.insert(motivoCierre!!)
                        }
                    }
                    operacion?.detallesOperacion?.results?.forEach { detalleOperacion ->
                        detalleOperacion?.ordenId = orden.ordenId
                        detalleOperacion?.operacionId = operacion.operacionId
                        detalleOperacionDao.insert(detalleOperacion!!)
                    }
                    if (!operacionesPendientes.contains("${operacion?.ordenId}|${operacion?.operacionId}")) {
                        operacion?.bi?.results?.forEach { baseInstalada ->
                            baseInstalada?.ordenId = orden.ordenId
                            baseInstalada?.operacionId = operacion.operacionId
                            baseInstaladaDao?.insert(baseInstalada!!)
                            baseInstalada?.original = true
                            baseInstaladaDao?.insert(baseInstalada!!)
                        }
                    }
                    operacion?.jerProAuto?.results?.forEach { jerProAuto ->
                        jerProAuto?.ordenId = orden.ordenId
                        jerProAuto?.operacionId = operacion.operacionId
                        jerProAutoDao.insert(jerProAuto!!)
                    }
                    operacion?.jerProNivel?.results?.forEach { jerProNivel ->
                        jerProNivel?.ordenId = orden.ordenId
                        jerProNivel?.operacionId = operacion.operacionId
                        jerProNivelDao.insert(jerProNivel!!)
                    }
                }
            }
            emit(odata)
        } else {
            error("Request failed, response body was null")
        }
    }

    fun getAyudaEtapaLinea() = flow {
        ayudaBusquedaDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta

        val response = SAPService().sapApi.getAyudaEtapaLinea()
        val odata = response.body()

        if (!response.isSuccessful) {
            error("Failed to get ayuda Etapa Linea")
        }

        if (odata != null) {
            odata.D?.results?.forEach { etapaLinea ->
                ayudaBusquedaDao.insert(
                    AyudaBusqueda(
                        campoAyuda = ETAPA_LINEA_AYUDA,
                        valorId = etapaLinea?.valorId!!,
                        descripcion = etapaLinea?.descripcion!!
                    )
                )
            }

            emit(odata)
        } else {
            error("Request failed, response body was null")
        }
    }

    fun getStock() = flow {
        stockDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta

        val response = SAPService().sapApi.getStock(TECNICO_ID, DEVICE_ID)
        val data = response.body()

        if (!response.isSuccessful) {
            error("Failed to get Stock")
        }

        if (data != null) {
            data.D?.stock?.results?.forEach { stock ->
                stockDao.insert(stock!!)
            }

            emit(data)
        } else {
            error("Request failed, response body was null")
        }
    }

    fun getMateriales() = flow {
        materialDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta
        materialesProyectoDao.deleteAll() // TODO borrar esta linea cuando hagamos la sincro delta

        val response = SAPService().sapApi.getMateriales(TECNICO_ID, DEVICE_ID)
        val data = response.body()

        if (!response.isSuccessful) {
            error("Failed to get Materiales")
        }

        if (data != null) {
            data.D?.materiales?.results?.forEach { material ->
                materialDao.insert(material!!)
                material.materialesProyecto?.results?.forEach { materialesProyecto ->
                    materialesProyectoDao.insert(materialesProyecto!!)
                }
            }

            emit(data)
        } else {
            error("Request failed, response body was null")
        }

        // TODO borrar esto cuando los datos que nos envie backend sean completos
        insertMockData()
    }

    private fun insertMockData() {
        // añadir material "BOT STD" a la tabla de materiales
        materialDao.insert(Material("100002339", "", "", "", 0, 0, "", "2000110005007"))
        // que el hijo de "BOT STD" pueda ser HC_DAVID
        jerProAutoDao.insert(
            JerProAuto(
                ordenId = "050000025769",
                operacionId = "0010",
                jerarquiaId = "2000110008004",
                tipoActividadId = "",
                claveModId = "",
                jerarquiaPadreId = "2000110005007",
                jerarquiaProDesc = "",
                jerarquiaPadreDesc = ""
            )
        )

        // que el HC_DAVID se pueda insertar en nivel 1
        jerProNivelDao.insert(
            JerProNivel(
                ordenId = "050000025769",
                operacionId = "0010",
                jerarquiaProId = "2000110008004",
                tipoActividadId = "",
                claveModId = "",
                jerarquiaProDesc = "",
                nivel = "1"
            )
        )

        // que el HC_DAVID se pueda insertar en nivel 0
        jerProNivelDao.insert(
            JerProNivel(
                ordenId = "050000025769",
                operacionId = "0010",
                jerarquiaProId = "2000110008004",
                tipoActividadId = "",
                claveModId = "",
                jerarquiaProDesc = "",
                nivel = "0"
            )
        )

        // que exista un stock de HC_DAVID que tenga el tag físico que tenemos
        stockDao.insert(
            Stock(
                materialId = "100005107",
                centroId = "E036",
                almacenId = "W001",
                numeroSerieId = "000100100000518750",
                numeroSerieFabId = "820717577",
                numeroPiezaId = "E004015056312AC7",
                lote = "USADO",
                textoMaterial = "HC_DAVID GREEN",
                statusSistema = "En almacén",
                cantidad = 1,
                denominacionTipo = "519051"
            )
        )

        // materiales que faltan para hacer validaciones BI finalizar
        // HEINEKEN CANARIAS (la raíz de nivel 1 que no se ve)
        materialDao.insert(Material("200000000140273", "", "", "", 8, 6, "", "100"))
        // Col CFT peltier original cromo HE
        materialDao.insert(Material("100001270", "", "", "", 2, 0, "", "101"))
        // HC_COLUMNA HEINEKEN FOUNTAIN DAVID
        materialDao.update(Material("100005009", "No", "ZMM_ZNBW", "", 2, 2, "No", "2000110005006"))
        // HC_DAVID GREEN
        materialDao.update(Material("100005107", "No", "ZMM_ZNBW", "", 5, 0, "Sí", "2000110008004"))
    }

    fun sendUserAndToken(
        userRequest: UserRequest,
        cb: SAPService.CallbackResponse<String>
    ) {
        SAPService().sapApi.sendUserAndToken(userRequest)
            .enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        cb.onResponse(response.body().toString())
                    } else {
                        cb.onFailure(Throwable(response.message()), response)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    cb.onFailure(t)
                }
            })
    }

    suspend fun getAyudaEtapaLineaLocal(): List<AyudaBusqueda> {
        return ayudaBusquedaDao.getEtapaLinea()
    }

    suspend fun getMotivoCierreByOrden(ordenId: String, operacionId: String): List<MotivoCierre> {
        return motivoCierreDao.getMotivoCierreByOrden(ordenId, operacionId)
    }

    suspend fun getStocksMontables(motivoCierre: MotivoCierre, idPadre: String, nivel: Int): List<Stock> {
        return stockDao.getStocksMontables(
            motivoCierre.ordenId,
            motivoCierre.operacionId,
            motivoCierre.motivoCierre,
            motivoCierre.tipoActividadId,
            motivoCierre.claveModId,
            motivoCierre.etapaLinea,
            idPadre,
            nivel
        )
    }
}