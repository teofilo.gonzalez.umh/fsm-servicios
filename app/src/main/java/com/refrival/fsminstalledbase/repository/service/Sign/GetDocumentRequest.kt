package com.refrival.fsminstalledbase.repository.service.Sign

import org.simpleframework.xml.*


@Root(name = "soap12:Envelope")
@NamespaceList(
    Namespace(
        reference = "http://www.w3.org/2001/XMLSchema-instance",
        prefix = "xsi"
    ),
    Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
    Namespace(
        reference = "http://www.w3.org/2003/05/soap-envelope",
        prefix = "soap12"
    ),
)
data class GetDocumentRequest(
    @field:Element(name = "soap12:Body", required = false)
    val body: GetDocumentRequestBody
)

@Root(name = "soap12:Body", strict = false)
data class GetDocumentRequestBody(
    @field:Element(name = "getDocument", required = false)
    val getDocument: GetDocumentRequestModel
)

@Namespace(reference = "http://mobilesign.com/")
data class GetDocumentRequestModel(
    @field:Element(name = "codigoEmpresa", required = false)
    val codigoEmpresa: String,

    @field:Element(name = "login", required = false)
    val login: String,

    @field:Element(name = "password", required = false)
    val password: String,

    @field:Element(name = "idioma", required = false)
    val idioma: String,

    @field:Element(name = "date", required = false)
    val date: String,

    @field:Element(name = "idDocumento", required = false)
    val idDocumento: String
)
