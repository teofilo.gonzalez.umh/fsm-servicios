package com.refrival.fsminstalledbase.repository.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.refrival.fsminstalledbase.repository.model.MotivoCierre

class Converters {
    /**
     * Generic function to convert to string from object
     */
    private inline fun <reified T> toString(value: T?): String? {
        return if (value != null) Gson().toJson(value, object : TypeToken<T>(){}.type) else null
    }

    /**
     * Generic function to convert to object from string
     */
    private inline fun <reified T> fromString(value: String?): T? {
        return if (value != null) Gson().fromJson<T>(value, object : TypeToken<T>() {}.type) else null
    }

    @TypeConverter
    fun fromMotivoCierre(value: MotivoCierre?): String? = toString(value)

    @TypeConverter
    fun toMotivoCierre(value: String?): MotivoCierre? = fromString(value)
}