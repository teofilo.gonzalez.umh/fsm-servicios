package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity

@Entity(tableName = "ayuda_busqueda_table", primaryKeys= [ "campoAyuda", "valorId" ] )
data class AyudaBusqueda(
    var campoAyuda: String,

    var valorId: String,

    var descripcion: String,
)