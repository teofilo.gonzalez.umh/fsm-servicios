package com.refrival.fsminstalledbase.repository.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.Operacion
import com.refrival.fsminstalledbase.repository.model.enums.OperacionStatus

@Dao
abstract class OperacionDao {
    @Insert
    abstract fun insert(operacion: Operacion)

    @Delete
    abstract fun delete(operacion: Operacion)

    @Update
    abstract fun update(operacion: Operacion)

    @Query( "UPDATE operacion_table SET status=:status WHERE ordenId=:ordenId AND operacionId=:operacionId")
    abstract suspend fun markStart(ordenId: String, operacionId: String, status: OperacionStatus = OperacionStatus.STARTED)

    @Query("SELECT * FROM operacion_table")
    abstract fun getAll(): LiveData<List<Operacion>>

    @Query("SELECT * FROM operacion_table WHERE " +
            "(:todo = 1 AND status = 'TODO') OR " +
            "(:started = 1 AND status = 'STARTED') OR " +
            "(:completed = 1 AND status = 'COMPLETED') OR " +
            "(:submitted = 1 AND status = 'SUBMITTED') OR " +
            "(:error = 1 AND status = 'ERROR')")
    abstract fun getOperationFiltered(
        todo: Boolean,
        started: Boolean,
        completed: Boolean,
        submitted: Boolean,
        error: Boolean): LiveData<List<Operacion>>

    @Query("SELECT * FROM operacion_table WHERE status=:status")
    abstract fun getAllTODO(status: OperacionStatus = OperacionStatus.TODO): List<Operacion>

    @Query("SELECT * FROM operacion_table WHERE status != :status")
    abstract fun getAllNotTODO(status: OperacionStatus = OperacionStatus.TODO): List<Operacion>

    @Query("SELECT * FROM operacion_table WHERE ordenId=:ordenId AND operacionId=:operacionId")
    abstract suspend fun getById(ordenId: String, operacionId: String): Operacion
}
