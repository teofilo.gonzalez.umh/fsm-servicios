package com.refrival.fsminstalledbase.repository.model.enums

enum class OperacionStatus {
    TODO,
    STARTED,
    COMPLETED,
    SUBMITTED,
    ERROR
}