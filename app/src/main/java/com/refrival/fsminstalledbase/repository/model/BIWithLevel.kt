package com.refrival.fsminstalledbase.repository.model

import androidx.room.Embedded

class BIWithLevel {
    @Embedded
    var baseInstalada: BaseInstalada? = null

    var level: Int = -1

    var esDesmontable: Boolean = false
}
