package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.Stock
import org.intellij.lang.annotations.Language

@Dao
abstract class StockDao {
    @Insert
    abstract fun insert(stock: Stock)

    @Delete
    abstract fun delete(stock: Stock)

    @Update
    abstract fun update(stock: Stock)

    @Query(
        """SELECT * FROM stock_table
                        where stock_table.materialId in ($SELECT_MATERIALES_TRATABLES_POR_MOTIVO_CIERRE)
                        and stock_table.materialId in ($SELECT_MATERIALES_PROYECTO_TRATABLES_POR_ORDEN)
                        and (:nivel = 0 or stock_table.materialId in ($SELECT_MATERIALES_QUE_PUEDEN_SER_HIJOS_DE_NODO_PADRE))
                        and stock_table.materialId in ($SELECT_MATERIALES_INSERTABLES_AL_NIVEL)
    """
    )
    abstract suspend fun getStocksMontables(
        ordenId: String,
        operacionId: String,
        motivoCierre: String,
        tipoActividadId: String,
        claveModId: String,
        etapaLinea: String,
        idPadre: String,
        nivel: Int
    ): List<Stock>

    @Query("DELETE FROM stock_table")
    abstract fun deleteAll()

    companion object {
        @Language("RoomSql")
        const val SELECT_MATERIALES_PROYECTO_TRATABLES_POR_ORDEN = """
            select distinct materiales_proyecto_table.materialId from operacion_table 
                                    inner join materiales_proyecto_table on materiales_proyecto_table.proyectoId = operacion_table.proyectoId
                                    where operacion_table.ordenId == :ordenId AND operacion_table.operacionId == :operacionId 
        """

        @Language("RoomSql")
        const val SELECT_MATERIALES_TRATABLES_POR_MOTIVO_CIERRE = """
            SELECT DISTINCT material_table.materialId from motivo_cierre_table 
                                    inner join material_table on material_table.jerarquiaId = motivo_cierre_table.jerarquiaId
                                    WHERE motivo_cierre_table.ordenId == :ordenId AND 
                                        motivo_cierre_table.operacionId == :operacionId AND
                                        motivo_cierre_table.motivoCierre == :motivoCierre AND
                                        motivo_cierre_table.tipoActividadId == :tipoActividadId AND
                                        motivo_cierre_table.claveModId == :claveModId AND
                                        motivo_cierre_table.etapaLinea == :etapaLinea
        """

        @Language("RoomSql")
        const val SELECT_MATERIALES_QUE_PUEDEN_SER_HIJOS_DE_NODO_PADRE = """
            select distinct material_hijo.materialId from jer_pro_auto_table 
            inner join material_table as material_padre on jer_pro_auto_table.jerarquiaPadreId = material_padre.jerarquiaId
            inner join material_table as material_hijo on jer_pro_auto_table.jerarquiaId = material_hijo.jerarquiaId
            inner join base_instalada_table on base_instalada_table.componente = material_padre.materialId
            where base_instalada_table.ordenId = :ordenId
                and base_instalada_table.operacionId = :operacionId
                and base_instalada_table.idNodo = :idPadre
                and base_instalada_table.original = 0
                and jer_pro_auto_table.ordenId = :ordenId
                and jer_pro_auto_table.operacionId = :operacionId
        """

        @Language("RoomSql")
        const val SELECT_MATERIALES_INSERTABLES_AL_NIVEL = """
            select distinct material_table.materialId from jer_pro_nivel_table
            inner join material_table on jer_pro_nivel_table.jerarquiaProId =  material_table.jerarquiaId
            where jer_pro_nivel_table.nivel == :nivel and jer_pro_nivel_table.ordenId = :ordenId and jer_pro_nivel_table.operacionId = :operacionId
        """
    }
}
