package com.refrival.fsminstalledbase.repository.service.Sign

import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

private const val BASE_URL = "https://integration2.3gmobilesign.com/"

var strategy = AnnotationStrategy()
var serializer = Persister(strategy)
var xmlFactory: SimpleXmlConverterFactory = SimpleXmlConverterFactory.createNonStrict(serializer)

private val retrofit = Retrofit.Builder()
    .addConverterFactory(xmlFactory)
    .baseUrl(BASE_URL)
    .build()

interface SignApiService {
    @Headers("Content-Type: application/soap+xml")
    @POST("mobilesignws.asmx")
    fun sendDocumentRich(@Body body: SendDocumentRichRequest) : Call<SendDocumentRichResponse>

    @Headers("Content-Type: application/soap+xml")
    @POST("mobilesignws.asmx")
    fun getDocument(@Body body: GetDocumentRequest) : Call<GetDocumentResponse>
}

object SignApi {
    val retrofitService : SignApiService by lazy { retrofit.create(SignApiService::class.java) }
}