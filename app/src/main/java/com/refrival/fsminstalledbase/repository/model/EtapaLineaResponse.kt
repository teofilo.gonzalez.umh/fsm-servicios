package com.refrival.fsminstalledbase.repository.model

import com.squareup.moshi.Json

data class EtapaLineaResponse(

	@Json(name="d")
	val D: DEtapa? = null
)

data class DEtapa(

	@Json(name="results")
	val results: List<ResultsItemEtapa?>? = null
)

data class ResultsItemEtapa(

	@Json(name="Descripcion")
	val descripcion: String? = null,

	@Json(name="ValorId")
	val valorId: String? = null,
)
