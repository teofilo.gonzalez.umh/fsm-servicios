package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "jer_pro_nivel_table", // TODO activar esta linea cuando lo miremos con Juan porque esta petando el UNIQUE CONSTRAINT:  primaryKeys= [ "ordenId", "operacionId", "jerarquiaProId", "tipoActividadId", "claveModId" ]
)
data class JerProNivel(
	// TODO borrar el campo id cuando tengamos resuelto las primeryKeys
	@PrimaryKey(autoGenerate = true)
	var id: Long? = null,

	@Json(name="OrdenId")
	var ordenId: String,

	var operacionId: String = "",

	@Json(name="JerarquiaProId")
	var jerarquiaProId: String,

	@Json(name="TipoActividadId")
	var tipoActividadId: String,

	@Json(name="ClaveModId")
	var claveModId: String,

	@Json(name="JerarquiaProDesc")
	var jerarquiaProDesc: String,

	@Json(name="Nivel")
	var nivel: String
)
