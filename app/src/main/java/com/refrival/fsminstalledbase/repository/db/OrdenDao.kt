package com.refrival.fsminstalledbase.repository.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.Orden

@Dao
abstract class OrdenDao {

    @Query("SELECT ServicioEmergencia FROM orden_table WHERE ordenId == :ordenId AND operacionId == :operacionId")
    abstract fun getEmergencyService(ordenId: String, operacionId: String) : LiveData<String>

    @Query("SELECT FueraHorario FROM orden_table WHERE ordenId == :ordenId AND operacionId == :operacionId")
    abstract fun getOutOfTime(ordenId: String, operacionId: String) : LiveData<String>

    @Query("SELECT OrdenDesc FROM orden_table WHERE ordenId == :ordenId")
    abstract fun getOrdenDescription(ordenId: String) : LiveData<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(orden: Orden)

    @Delete
    abstract fun delete(orden: Orden)

    @Update
    abstract fun update(orden: Orden)

    @Query("DELETE FROM orden_table")
    abstract fun deleteAll()
}
