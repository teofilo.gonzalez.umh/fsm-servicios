package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.MaterialesProyecto

@Dao
abstract class MaterialesProyectoDao {
    @Insert
    abstract fun insert(materialesProyecto: MaterialesProyecto)

    @Delete
    abstract fun delete(materialesProyecto: MaterialesProyecto)

    @Update
    abstract fun update(materialesProyecto: MaterialesProyecto)

    @Query("DELETE FROM materiales_proyecto_table")
    abstract fun deleteAll()
}
