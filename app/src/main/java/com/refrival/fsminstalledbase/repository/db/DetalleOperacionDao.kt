package com.refrival.fsminstalledbase.repository.db

import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.DetalleOperacion

@Dao
abstract class DetalleOperacionDao {
    @Insert
    abstract fun insert(detalleOperacion: DetalleOperacion)

    @Delete
    abstract fun delete(detalleOperacion: DetalleOperacion)

    @Update
    abstract fun update(detalleOperacion: DetalleOperacion)

    @Query("DELETE FROM detalle_operacion_table")
    abstract fun deleteAll()
}
