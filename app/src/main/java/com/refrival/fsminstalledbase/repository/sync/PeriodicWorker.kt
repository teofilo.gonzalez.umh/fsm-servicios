package com.refrival.fsminstalledbase.repository.sync

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.refrival.fsminstalledbase.utils.UNIQUE_PERIODIC_WORK_NAME
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import java.util.concurrent.TimeUnit

@HiltWorker
class PeriodicWorker @AssistedInject constructor(
    @Assisted private val appContext: Context,
    @Assisted workerParams: WorkerParameters,
) : Worker(appContext, workerParams)  {
    override fun doWork(): Result {
        SyncWorker.enqueue(appContext)

        return Result.success()
    }

    companion object {
        fun enqueue(context: Context) {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val periodicWorkRequest = PeriodicWorkRequestBuilder<PeriodicWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(UNIQUE_PERIODIC_WORK_NAME, ExistingPeriodicWorkPolicy.REPLACE, periodicWorkRequest)
        }
    }
}