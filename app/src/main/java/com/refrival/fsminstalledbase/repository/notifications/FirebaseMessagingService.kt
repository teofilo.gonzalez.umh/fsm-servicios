package com.refrival.fsminstalledbase.repository.notifications

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.refrival.fsminstalledbase.repository.db.RefrivalDatabase
import com.refrival.fsminstalledbase.repository.model.UserRequest
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import com.refrival.fsminstalledbase.repository.service.sap.SAPService
import com.refrival.fsminstalledbase.repository.sync.SyncWorker
import com.refrival.fsminstalledbase.utils.PrefManager
import com.refrival.fsminstalledbase.utils.SAP_USER_TECNICO_1
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject lateinit var sapRepository: SAPRepository

    override fun onNewToken(token: String) {
        Log.w(TAG, "Refreshed token: $token")
        PrefManager(applicationContext).saveFirebaseToken(token)

        if (RefrivalDatabase.getInstance(applicationContext).userDao().get().value?.userId != null) {
            val userRequest = UserRequest()
            userRequest.TecnicoId = SAP_USER_TECNICO_1 //TODO -> Change for user DB
            userRequest.DeviceId = PrefManager(applicationContext).getFirebaseToken
            sentUserAndToken(userRequest)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        SyncWorker.enqueue(applicationContext)
    }

    private fun sentUserAndToken(userRequest: UserRequest) {
        sapRepository.sendUserAndToken(userRequest, object : SAPService.CallbackResponse<String>{
            override fun onResponse(response: String) {
                Log.w("TAG", "USER-TOKEN: $response")
            }

            override fun onFailure(t: Throwable, res: Response<*>?) {
                Log.e("TAG", "USER-TOKEN - Error: ${t.localizedMessage}")
            }
        })
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}