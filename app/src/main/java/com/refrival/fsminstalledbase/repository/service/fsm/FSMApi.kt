package com.refrival.fsminstalledbase.repository.service.fsm

import com.refrival.fsminstalledbase.repository.model.User
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface FSMApi {

    @FormUrlEncoded
    @POST("/api/oauth2/v1/token")
    fun login(
        @Field("grant_type") grant_type: String,
        @Field("response_type") response_type: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<User>
}