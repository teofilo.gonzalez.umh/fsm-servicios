package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "detalle_operacion_table", primaryKeys= [ "ordenId", "operacionId", "posicion" ] )
data class DetalleOperacion(
	@Json(name="OrdenId")
	var ordenId: String,

	@Json(name="OperacionId")
	var operacionId: String,

	@Json(name="Posicion")
	var posicion: String,

	@Json(name="NumSerie")
	var numSerie: String,

	@Json(name="MaterialId")
	var materialId: String,

	@Json(name="AlmacenId")
	var almacenId: String,

	@Json(name="LoteId")
	var loteId: String,

	@Json(name="TipoPosicionId")
	var tipoPosicionId: String,

	@Json(name="CentroId")
	var centroId: String,

	@Json(name="MaterialDesc")
	var materialDesc: String,

	@Json(name="EstadoId")
	var estadoId: String,

	@Json(name="Cantidad")
	var cantidad: String,

	@Json(name="Unidad")
	var unidad: String,
)
