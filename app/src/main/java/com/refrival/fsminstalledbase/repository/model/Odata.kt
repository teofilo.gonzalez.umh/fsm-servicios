package com.refrival.fsminstalledbase.repository.model

import androidx.room.Embedded
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import java.util.*

data class Odata(
    @PrimaryKey
    var id: String = UUID.randomUUID().toString(),

    @Json(name = "d")
    var D: D? = null
)

data class D(
    @Json(name = "Ordenes")
    @Embedded(prefix = "ordenes_")
    var ordenes: Ordenes? = null,

    @Json(name = "DeviceId")
    var deviceId: String? = null,

    @Json(name = "TecnicoId")
    var tecnicoId: String? = null,

    @Json(name = "TecnicoDesc")
    var tecnicoDesc: String? = null,

    @Json(name="Stock")
    val stock: StockOdata? = null,

    @Json(name="Materiales")
    val materiales: MaterialesOdata? = null
)

data class Ordenes(
    @Json(name = "results")
    var results: List<Orden?>? = null
)

data class StockOdata(
    @Json(name="results")
    val results: List<Stock?>? = null
)

data class MaterialesOdata(
    @Json(name="results")
    val results: List<Material?>? = null
)