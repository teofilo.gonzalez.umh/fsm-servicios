package com.refrival.fsminstalledbase.repository.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.BIReg
import com.refrival.fsminstalledbase.repository.model.BIWithLevel
import com.refrival.fsminstalledbase.repository.model.BaseInstalada
import com.refrival.fsminstalledbase.utils.ACCION_BI_DESMONTAR
import com.refrival.fsminstalledbase.utils.ACCION_BI_MODIFICAR
import com.refrival.fsminstalledbase.utils.ACCION_BI_SELECCIONAR
import org.intellij.lang.annotations.Language

@Dao
abstract class BaseInstaladaDao {
    @Insert
    abstract fun insert(baseInstalada: BaseInstalada)

    @Delete
    abstract fun delete(baseInstalada: BaseInstalada)

    @Update
    abstract fun update(baseInstalada: BaseInstalada)

    @Query("SELECT * from base_instalada_table WHERE accion != ''")
    abstract suspend fun getComponenteEditado(): List<BaseInstalada>

    @Query("SELECT * from base_instalada_table WHERE ordenId == :ordenId AND operacionId == :operacionId AND original == :original")
    abstract suspend fun getByOrdenOperacion(
        ordenId: String,
        operacionId: String,
        original: Boolean
    ): List<BaseInstalada>

    @Query("DELETE FROM base_instalada_table WHERE ordenId == :ordenId AND operacionId == :operacionId AND original == :original")
    abstract suspend fun deleteBI(ordenId: String, operacionId: String, original: Boolean)

    @Query("DELETE FROM base_instalada_table WHERE ordenId == :ordenId AND operacionId == :operacionId")
    abstract fun deleteAllByOrdenOperacion(ordenId: String, operacionId: String)

    @Query(
        """
        with recursive under_root as (
          select  0 level, base_instalada_table.*, 0 esDesmontable from base_instalada_table where idPadre == '' and base == '' and ordenId == :ordenId and operacionId == :operacionId and original = 1
          union all
          select under_root.level + 1, base_instalada_table.*, 0 esDesmontable
          from base_instalada_table
          join under_root on base_instalada_table.idPadre = under_root.idNodo
          where base_instalada_table.original = 1  and base_instalada_table.ordenId == :ordenId and base_instalada_table.operacionId == :operacionId
          order by 1 desc
        )
        select * from under_root where under_root.level > 1
    """
    )
    abstract fun getTreeBaseInstaladaOriginal(
        ordenId: String,
        operacionId: String
    ): LiveData<List<BIWithLevel>>

    @Query(
        """
        with recursive under_root as (
          select  0 level, base_instalada_table.*, 0 esDesmontable from base_instalada_table where idPadre == '' and base == '' and ordenId == :ordenId and operacionId == :operacionId and original == 0
          union all
          select under_root.level + 1, base_instalada_table.*, 
                    base_instalada_table.componente in (${StockDao.SELECT_MATERIALES_TRATABLES_POR_MOTIVO_CIERRE}) esDesmontable
          from base_instalada_table
          join under_root on base_instalada_table.idPadre = under_root.idNodo
          where base_instalada_table.original = 0  and base_instalada_table.ordenId == :ordenId and base_instalada_table.operacionId == :operacionId
          order by 1 desc
        )
        select * from under_root where under_root.level > 1
    """
    )
    abstract fun getTreeBaseInstalada(
        ordenId: String,
        operacionId: String,
        motivoCierre: String,
        tipoActividadId: String,
        claveModId: String,
        etapaLinea: String
    ): LiveData<List<BIWithLevel>>

    @Query(
        """
        with recursive under_root as (
          select  0 level, base_instalada_table.* from base_instalada_table where idPadre == '' and base == '' and ordenId == :ordenId and operacionId == :operacionId and original == 0
          union all
          select under_root.level + 1, base_instalada_table.*
          from base_instalada_table
          join under_root on base_instalada_table.idPadre = under_root.idNodo
          where base_instalada_table.original = 0  and base_instalada_table.ordenId == :ordenId and base_instalada_table.operacionId == :operacionId
          order by 1 desc
        )
        select * from under_root where under_root.level > 1
    """
    )
    abstract fun getTreeBaseInstaladaReg(
        ordenId: String,
        operacionId: String,
    ): LiveData<List<BIReg>>

    @Query(
        """
            update base_instalada_table
            set accion = '$ACCION_BI_MODIFICAR', textoComponente = :textoComponente, numSerie = :numSerie, numPiezaFabricante = :tag
            where ordenId = :ordenId and operacionId = :operacionId and original = :original and idNodo = :idNodo
    """
    )
    abstract fun modificarElemento(
        idNodo: String,
        ordenId: String,
        operacionId: String,
        textoComponente: String,
        numSerie: String,
        tag: String,
        original: Boolean = false
    )

    @Query(
        """
            update base_instalada_table
            set accion = '$ACCION_BI_DESMONTAR'
            where ordenId = :ordenId and operacionId = :operacionId and original = :original and idNodo in ($SELECT_IDS_DESCENDIENTES_DE_NODO)
    """
    )
    abstract fun desmontarElemento(
        idNodo: String,
        ordenId: String,
        operacionId: String,
        original: Boolean = false
    )

    @Query(
        """
        with recursive under_root as (
          select  0 level, base_instalada_table.* from base_instalada_table where idPadre == '' and base == '' and ordenId == :ordenId and operacionId == :operacionId and original == :original
          union all
          select under_root.level + 1, base_instalada_table.*
          from base_instalada_table
          join under_root on base_instalada_table.idPadre = under_root.idNodo
          where base_instalada_table.original = :original  and base_instalada_table.ordenId == :ordenId and base_instalada_table.operacionId == :operacionId and level < 1
          order by 1 desc
        )
        select * from under_root where level == 1
        """
    )
    abstract suspend fun getRoot(ordenId: String, operacionId: String, original: Boolean): BaseInstalada

    @Query(
        """
        select base_instalada_table.textoComponente,
                base_instalada_table.componente,
                base_instalada_table.idNodo, 
                count(bi_hijos.idNodo) numNodos, 
                material_table.nodosMinimos,
                material_table.nodosMaximos,
                base_instalada_table.idNodo = ($SELECT_ID_NODO_NIVEL_MENOS_1) esNivelMenos1
        from base_instalada_table
        left join base_instalada_table as bi_hijos on bi_hijos.idPadre = base_instalada_table.idNodo and  bi_hijos.original = 0 -- TODO aquí se tendría que poner que los nodos que estan desmontados o con cantidad 0 o lo que sea no los cuente como hijos
        inner join material_table on base_instalada_table.componente = material_table.materialId
        where base_instalada_table.ordenId = :ordenId 
                and base_instalada_table.operacionId = :operacionId
                and base_instalada_table.original = 0
                and base_instalada_table.idPadre != ''
                and base_instalada_table.accion != '$ACCION_BI_DESMONTAR' -- TODO aquí se tendría que poner que los nodos que estan desmontados o con cantidad 0 o lo que sea no hace falta hacer ninguna validacion
        group by base_instalada_table.idNodo
        having count(bi_hijos.idNodo) < material_table.nodosMinimos or  count(bi_hijos.idNodo) > material_table.nodosMaximos
        """
    )
    abstract suspend fun getBICheck(ordenId: String, operacionId: String, original: Boolean = false): List<BICheck>

    @Query(
        """
            update base_instalada_table
            set accion = '$ACCION_BI_SELECCIONAR'
            where ordenId = :ordenId and operacionId = :operacionId and original = :original and idNodo = :idNodo
    """
    )
    abstract fun seleccionarElemento(idNodo: String, ordenId: String, operacionId: String, original: Boolean = false)

    companion object {
        @Language("RoomSql")
        const val SELECT_IDS_DESCENDIENTES_DE_NODO = """
             with recursive under_root as (
              select  0 level, base_instalada_table.idNodo from base_instalada_table where idNodo = :idNodo and ordenId == :ordenId and operacionId == :operacionId and original = :original
              union all
              select under_root.level + 1, base_instalada_table.idNodo
              from base_instalada_table
              join under_root on base_instalada_table.idPadre = under_root.idNodo
              where base_instalada_table.original = :original  and base_instalada_table.ordenId == :ordenId  and base_instalada_table.operacionId == :operacionId
              order by 1 desc
            )
            select idNodo from under_root
        """

        @Language("RoomSql")
        const val SELECT_ID_NODO_NIVEL_MENOS_2 = """
            select idNodo 
            from base_instalada_table 
            where base_instalada_table.ordenId = :ordenId 
                and base_instalada_table.operacionId = :operacionId
                and base_instalada_table.original = :original 
                and idPadre = ''
                and base = ''
        """

        @Language("RoomSql")
        const val SELECT_ID_NODO_NIVEL_MENOS_1 = """
            select idNodo 
            from base_instalada_table 
            where base_instalada_table.ordenId = :ordenId 
                and base_instalada_table.operacionId = :operacionId
                and base_instalada_table.original = :original 
                and idPadre = ($SELECT_ID_NODO_NIVEL_MENOS_2)
        """
    }

    data class BICheck (
        val textoComponente: String,
        val nodosMinimos: Int,
        val nodosMaximos: Int,
        val esNivelMenos1: Boolean
    )
}