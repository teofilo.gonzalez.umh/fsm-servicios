package com.refrival.fsminstalledbase.repository.sync

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.refrival.fsminstalledbase.repository.service.sap.SAPRepository
import com.refrival.fsminstalledbase.utils.UNIQUE_SYNC_WORK_NAME
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.collect

@HiltWorker
class SyncWorker  @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val sapRepository: SAPRepository
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        return try {
            syncWithServer()

            Result.success()
        } catch (e: Exception) {
            // Send the error message
            Result.failure(Data.Builder().putString("errorMsg", e.message).build())
        }
    }

    private suspend fun syncWithServer() {
        receiveDbUpdates()
    }

    private suspend fun receiveDbUpdates() {
        sapRepository.getOdata().collect()
        setProgressAsync(Data.Builder().putInt("progress", 40).build())
        sapRepository.getAyudaEtapaLinea().collect()
        setProgressAsync(Data.Builder().putInt("progress", 50).build())
        sapRepository.getStock().collect()
        setProgressAsync(Data.Builder().putInt("progress", 60).build())
        sapRepository.getMateriales().collect()
        setProgressAsync(Data.Builder().putInt("progress", 70).build())
    }

    companion object {
        fun enqueue(context: Context) {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val syncWorkRequest = OneTimeWorkRequestBuilder<SyncWorker>()
                .setConstraints(constraints)
                .build()

            WorkManager
                .getInstance(context)
                .enqueueUniqueWork(UNIQUE_SYNC_WORK_NAME, ExistingWorkPolicy.APPEND_OR_REPLACE, syncWorkRequest)
        }
    }
}