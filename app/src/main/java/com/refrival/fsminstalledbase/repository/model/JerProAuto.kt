package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "jer_pro_auto_table", // TODO activar esta linea cuando lo miremos con Juan porque esta petando el UNIQUE CONSTRAINT: primaryKeys= [ "ordenId", "operacionId", "jerarquiaId", "tipoActividadId", "claveModId" ]
)
data class JerProAuto(
	// TODO borrar el campo id cuando tengamos resuelto las primeryKeys
	@PrimaryKey(autoGenerate = true)
	var id: Long? = null,

	@Json(name="OrdenId")
	var ordenId: String,

	var operacionId: String = "",

	@Json(name="JerarquiaId")
	var jerarquiaId: String,

	@Json(name="TipoActividadId")
	var tipoActividadId: String,

	@Json(name="ClaveModId")
	var claveModId: String,

	@Json(name="JerarquiaPadreId")
	var jerarquiaPadreId: String,

	@Json(name="JerarquiaProDesc")
	var jerarquiaProDesc: String,

	@Json(name="JerarquiaPadreDesc")
	var jerarquiaPadreDesc: String
)
