package com.refrival.fsminstalledbase.repository.model

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "base_instalada_table", primaryKeys= [ "ordenId", "operacionId", "idNodo", "original" ] )
data class BaseInstalada(
    var ordenId: String = "",

    var operacionId: String = "",

    @Json(name="IdNodo")
    var idNodo: String,

    var original: Boolean = false,

    @Json(name="Accion")
    var accion: String,

    @Json(name="BiId")
    var biId: String = "",

    @Json(name="ClvModUltAct")
    var clvModUltAct: String = "",

    @Json(name="IdExterno")
    var idExterno: String = "",

    @Json(name="Componente")
    var componente: String,

    @Json(name="FecUltClvMod")
    var fecUltClvMod: String = "",

    @Json(name="TextoComponente")
    var textoComponente: String,

    @Json(name="NumPiezaFabricante")
    var numPiezaFabricante: String,

    @Json(name="NumSerieFabricante")
    var numSerieFabricante: String = "",

    @Json(name="Descripcion")
    var descripcion: String = "",

    @Json(name="NumSerie")
    var numSerie: String,

    @Json(name="ClaveClasificacion")
    var claveClasificacion: String,

    @Json(name="FecUltLecTag")
    var fecUltLecTag: String = "",

    @Json(name="IdPadre")
    var idPadre: String,

    @Json(name="Base")
    var base: String = "",

    @Json(name="TipoBase")
    var tipoBase: String = "",

    @Json(name="TipoComponente")
    var tipoComponente: String,

    @Json(name="Cantidad")
    var cantidad: Int,

    @Json(name="InstalacionOrion")
    var instalacionOrion: String = "",

    @Json(name="Unidad")
    var unidad: String = "",

    @Json(name="TipoUltAct")
    var tipoUltAct: String = "",
)
