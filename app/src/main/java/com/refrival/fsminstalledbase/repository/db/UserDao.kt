package com.refrival.fsminstalledbase.repository.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.refrival.fsminstalledbase.repository.model.User

@Dao
abstract class UserDao {

    @Query("SELECT * FROM user_table WHERE userId = :userId")
    abstract fun getUser(userId: String) : LiveData<User>

    @Query("SELECT * FROM user_table")
    abstract fun get() : LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(user: User)

    @Delete
    abstract fun delete(user: User)

    @Update
    abstract fun update(user: User)
}