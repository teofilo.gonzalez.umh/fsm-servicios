package com.refrival.fsminstalledbase.repository.service.Sipay

import com.squareup.moshi.Json

data class AuthenticationResponse(
    val type: String,
    val code: String,
    val detail: String,
    val description: String,
    val payload: AuthenticationPayload,
    val uuid: String,
    @Json(name = "request_id") val requestId: String
)

data class AuthenticationPayload (
    val url: String
)
