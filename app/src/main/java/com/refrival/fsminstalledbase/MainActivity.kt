package com.refrival.fsminstalledbase

import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.refrival.fsminstalledbase.base.Base
import com.refrival.fsminstalledbase.ui.installedbase.InstalledBaseFragmentDirections
import com.refrival.fsminstalledbase.ui.signatureCheck.SignatureCheckFragmentDirections
import com.refrival.fsminstalledbase.utils.UtilsNetworks
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.apache.commons.codec.binary.Hex


@AndroidEntryPoint
class MainActivity : Base.BaseActivity(), NfcAdapter.ReaderCallback {

    companion object {
        const val UPDATE_INTERVAL = 5000L
    }

    interface IOnBackPressed {
        fun onBackPressed(): Boolean
    }

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var mNfcAdapter: NfcAdapter? = null
    private val viewModel: MainViewModel by viewModels()
    private val updateWidgetHandler = Handler(Looper.getMainLooper())
    private lateinit var headerView: View

    override fun getXmlLayout(): Int {
        return R.layout.activity_main
    }

    override fun init() {
        setSupportActionBar(toolbar)
        updateWidget()
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this)

        val drawerLayout: DrawerLayout = drawer_layout
        val navView: NavigationView = nav_view
        headerView = navView.getHeaderView(0)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.viewInstalledBaseFragment, R.id.operationsFragment), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        viewModel.getUser(this)
    }

    override fun initListeners() {
        viewModel.user.observe(this) {
            headerView.txtUserName.text = getString(R.string.user).plus(": ".plus(it?.account))
            headerView.txtUserID.text = getString(R.string.account).plus(": ".plus(it?.user))
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        updateWidgetHandler.postDelayed(updateWidgetRunnable, UPDATE_INTERVAL)
        if (mNfcAdapter != null) {
            val options = Bundle()
            // Work around for some broken Nfc firmware implementations that poll the card too fast
            options.putInt(NfcAdapter.EXTRA_READER_PRESENCE_CHECK_DELAY, 250)

            // Enable ReaderMode for all types of card and disable platform sounds
            mNfcAdapter!!.enableReaderMode(
                this,
                this,
                NfcAdapter.FLAG_READER_NFC_A or
                        NfcAdapter.FLAG_READER_NFC_B or
                        NfcAdapter.FLAG_READER_NFC_F or
                        NfcAdapter.FLAG_READER_NFC_V or
                        NfcAdapter.FLAG_READER_NFC_BARCODE,
                options
            )
        }
    }

    override fun onPause() {
        super.onPause()
        updateWidgetHandler.removeCallbacks(updateWidgetRunnable);
        mNfcAdapter?.disableReaderMode(this)
    }

    // This method is run in another thread when a card is discovered
    // !!!! This method cannot cannot direct interact with the UI Thread
    // Use `runOnUiThread` method to change the UI from this method
    override fun onTagDiscovered(tag: Tag?) {
        val sb = StringBuilder()
        val id = tag?.id
        sb.append(Hex.encodeHex(id))

        viewModel.setTagId(reverseHex(sb.toString()))
    }

    private fun reverseHex(s: String): String {
        val result = StringBuilder()

        var i = 0
        while (i <= s.length - 2) {
            result.append(StringBuilder(s.substring(i, i + 2)).reverse())
            i += 2
        }

        return result.reverse().toString()
    }

    private var updateWidgetRunnable: Runnable = Runnable {
        run {
            updateWidget()
            updateWidgetHandler.postDelayed(updateWidgetRunnable, UPDATE_INTERVAL)
        }
    }

    private fun updateWidget() {
        txtOnline.visibility = View.VISIBLE
        if (UtilsNetworks.isConnected(this)) {
            txtOnline.text = getString(R.string.online)
            txtOnline.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.valid
                )
            )
        } else {
            txtOnline.text = getString(R.string.offline)
            txtOnline.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.no_valid
                )
            )
        }
    }

    fun showButtonMaterial(show: Boolean) {
        if (show) buttonAddMaterial.show()
        else buttonAddMaterial?.hide()
    }

    fun onClickButtonMaterial() {
        if (buttonAddMaterial != null)
            buttonAddMaterial.setOnClickListener {
                findNavController(R.id.nav_host_fragment_content_main).navigate(InstalledBaseFragmentDirections.actionInstalledBaseFragmentToAddMaterialFragment(idPadre = "", nivel = 0))
            }
    }

    override fun onBackPressed() {
        val fragment = this.supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as? NavHostFragment
        val currentFragment = fragment?.childFragmentManager?.fragments?.get(0) as? IOnBackPressed
        currentFragment?.onBackPressed()?.takeIf { !it }?.let{
            findNavController(R.id.nav_host_fragment_content_main).navigate(SignatureCheckFragmentDirections.actionSignatureCheckFragmentToBaseInstaladaRegFragment())
        } ?: super.onBackPressed()
    }
}